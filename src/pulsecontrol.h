/***
  This file is part of pulsecontrol.

  Copyright 2006-2008 Lennart Poettering
  Copyright 2009 Colin Guthrie

  pulsecontrol is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  pulsecontrol is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pulsecontrol. If not, see <http://www.gnu.org/licenses/>.
***/

#ifndef pulsecontrol_h
#define pulsecontrol_h

#include <string>

#include <signal.h>
#include <string.h>

#include <libintl.h>

#include <gtkmm.h>
#include <gtkmm/buildable.h>

#include <pulse/pulseaudio.h>
#include <pulse/glib-mainloop.h>

#include <iostream>

/* Can be removed when PulseAudio 0.9.23 or newer is required */
#ifndef PA_VOLUME_UI_MAX
# define PA_VOLUME_UI_MAX (pa_sw_volume_from_dB(+11.0))
#endif

#define HAVE_SOURCE_OUTPUT_VOLUMES PA_CHECK_VERSION(0,99,0)
#define HAVE_EXT_DEVICE_RESTORE_API PA_CHECK_VERSION(0,99,0)

#define PEAKS_RATE 144

enum SinkInputType {
    SINK_INPUT_NOTHING = 0,
    SINK_INPUT_CLIENT = 1 << 0,
    SINK_INPUT_VIRTUAL = 1 << 1,
    SINK_INPUT_ROLE = 1 << 2,
};

enum SinkType {
    SINK_NOTHING = 0,
    SINK_HARDWARE = 1 << 0,
    SINK_VIRTUAL = 1 << 1,
};

enum SourceOutputType {
    SOURCE_OUTPUT_NOTHING = 0,
    SOURCE_OUTPUT_CLIENT = 1 << 0,
    SOURCE_OUTPUT_VIRTUAL = 1 << 1,
};

enum SourceType {
    SOURCE_NOTHING = 0,
    SOURCE_HARDWARE = 1 << 0,
    SOURCE_VIRTUAL = 1 << 1,
    SOURCE_MONITOR = 1 << 2,
};

class DeviceComboBoxItem {
public:
    Glib::ustring name;
    Glib::ustring description;
    uint32_t index;
    uint32_t priority;
    gboolean availability;
};

#include "mainwindow.h"

pa_context* get_context(void);
void show_error(Gtk::Widget *w, const char *txt);

MainWindow* pulsecontrol_get_window(pa_glib_mainloop *m, bool maximize, bool retry, int tab_number);

#ifdef HAVE_PULSE_MESSAGING_API
std::string card_message_handler_path(const std::string& name);
std::string card_bluez_message_handler_path(const std::string& name);
#endif

#endif
