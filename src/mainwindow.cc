/***
  This file is part of pulsecontrol.

  Copyright 2006-2008 Lennart Poettering
  Copyright 2009 Colin Guthrie

  pulsecontrol is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  pulsecontrol is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pulsecontrol. If not, see <http://www.gnu.org/licenses/>.
***/

#include <unordered_set>

#include "mainwindow.h"

#include "frames/frame-playback.h"
#include "frames/frame-recording.h"
#include "frames/frame-sink.h"
#include "frames/frame-source.h"
#include "frames/frame-config.h"

#include "i18n.h"


MainWindow::MainWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x) :
    Gtk::Window(cobject),
    moduleDeviceManagerIndex(-1),
    m_connected(false),
    m_config_filename(NULL) {
    connectingLabel = x->get_widget<Gtk::Label>("connectingLabel");
    viewStack = x->get_widget<Gtk::Widget>("view_stack");

    auto event_controller_key = Gtk::EventControllerKey::create();
    event_controller_key->signal_key_pressed().connect(sigc::mem_fun(*this, &MainWindow::on_key_press_event), false);
    this->add_controller(event_controller_key);

    GKeyFile* config = g_key_file_new();
    g_assert(config);
    GKeyFileFlags flags = (GKeyFileFlags)( G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS);
    GError *err = NULL;
    m_config_filename = g_strconcat(g_get_user_config_dir(), "/pulsecontrol.ini", NULL);

    gint showSinkInputType = -1, showSourceOutputType = -1, showSinkType = -1, showSourceType = -1;
    char* lastActiveFrame = NULL;
    streamsBox = x->get_widget<Gtk::Box>("streamsBox");
    recsBox = x->get_widget<Gtk::Box>("recsBox");
    sourcesBox = x->get_widget<Gtk::Box>("sourcesBox");
    sinksBox = x->get_widget<Gtk::Box>("sinksBox");
    sinksBox = x->get_widget<Gtk::Box>("sinksBox");
    configBox = x->get_widget<Gtk::Box>("configBox");
    /* Load the GKeyFile from keyfile.conf or return. */
    if (g_key_file_load_from_file (config, m_config_filename, flags, &err)) {
        int width  = g_key_file_get_integer(config, "window", "width", NULL);
        int height = g_key_file_get_integer(config, "window", "height", NULL);

        int default_width, default_height;
        get_default_size(default_width, default_height);
        if (width >= default_width && height >= default_height)
            set_default_size(width, height);

        showSinkInputType = g_key_file_get_integer(config, "window", "sinkInputType", &err);
        if (err != NULL) {
            showSinkInputType = -1;
            g_error_free(err);
            err = NULL;
        }

        showSourceOutputType = g_key_file_get_integer(config, "window", "sourceOutputType", &err);
        if (err != NULL) {
            showSourceOutputType = -1;
            g_error_free(err);
            err = NULL;
        }

        showSinkType = g_key_file_get_integer(config, "window", "sinkType", &err);
        if (err != NULL) {
            showSinkType = -1;
            g_error_free(err);
            err = NULL;
        }

        showSourceType = g_key_file_get_integer(config, "window", "sourceType", &err);
        if (err != NULL) {
            showSourceType = -1;
            g_error_free(err);
            err = NULL;
        }
        lastActiveFrame = g_key_file_get_string(config, "window", "frame", &err);
        if (err != NULL) {
            showSourceType = -1;
            g_error_free(err);
            err = NULL;
        }
    } else {
        g_debug(_("Error reading config file %s: %s"), m_config_filename, err->message);
        g_error_free(err);
    }
    g_key_file_free(config);
    sinkInputFrame = FrameSinkInput::create(showSinkInputType);
    streamsBox->append(*sinkInputFrame);
    sourceOutputFrame = FrameSourceOutput::create(showSourceOutputType);
    recsBox->append(*sourceOutputFrame);
    sinkFrame = FrameSink::create(showSinkType);
    sinksBox->append(*sinkFrame);
    sourceFrame = FrameSource::create(showSourceType);
    sourcesBox->append(*sourceFrame);
    configFrame = FrameConfig::create();
    configBox->append(*configFrame);

    if (lastActiveFrame)
        adw_view_stack_set_visible_child_name(ADW_VIEW_STACK(viewStack->gobj()), lastActiveFrame);

    /* Hide first and show when we're connected */
    viewStack->hide();
    connectingLabel->show();
}

MainWindow* MainWindow::create(bool maximize) {
    MainWindow* w;
    Glib::RefPtr<Gtk::Builder> x = Gtk::Builder::create_from_resource("/pulsecontrol/ui/mainwindow.ui");
    w = Gtk::Builder::get_widget_derived<MainWindow>(x, "mainWindow");
    if (w && maximize)
        w->maximize();
    return w;
}

void MainWindow::on_realize() {
    Gtk::Window::on_realize();

    set_cursor(Gdk::Cursor::create("wait"));
}

bool MainWindow::on_key_press_event(guint keyval, guint keycode, Gdk::ModifierType state) {
    if ((state & Gdk::ModifierType::CONTROL_MASK) == Gdk::ModifierType::CONTROL_MASK) {
        switch (keyval) {
            case GDK_KEY_KP_1:
            case GDK_KEY_KP_2:
            case GDK_KEY_KP_3:
            case GDK_KEY_KP_4:
            case GDK_KEY_KP_5:
                selectTab(keyval - GDK_KEY_KP_0);
                return true;
            case GDK_KEY_1:
            case GDK_KEY_2:
            case GDK_KEY_3:
            case GDK_KEY_4:
            case GDK_KEY_5:
                selectTab(keyval - GDK_KEY_0);
                return true;
            case GDK_KEY_W:
            case GDK_KEY_Q:
            case GDK_KEY_w:
            case GDK_KEY_q:
                close();
                return true;
        }
    }
    return false;
}

MainWindow::~MainWindow() {
    GKeyFile* config = g_key_file_new();
    g_assert(config);

    int width, height;
    get_default_size(width, height);
    g_key_file_set_integer(config, "window", "width", width);
    g_key_file_set_integer(config, "window", "height", height);
    g_key_file_set_integer(config, "window", "sinkInputType", sinkInputFrame->getSinkInputTypeShown());
    g_key_file_set_integer(config, "window", "sourceOutputType", sourceOutputFrame->getSourceOutputTypeShown());
    g_key_file_set_integer(config, "window", "sinkType", sinkFrame->getSinkTypeShown());
    g_key_file_set_integer(config, "window", "sourceType", sourceFrame->getSourceTypeShown());
    g_key_file_set_string(config, "window", "frame", adw_view_stack_get_visible_child_name(ADW_VIEW_STACK(viewStack->gobj())));

    gsize filelen;
    GError *err = NULL;
    gchar *filedata = g_key_file_to_data(config, &filelen, &err);
    if (err) {
        show_error(this, _("Error saving preferences"));
        g_error_free(err);
        goto finish;
    }

    g_file_set_contents(m_config_filename, filedata, filelen, &err);
    g_free(filedata);
    if (err) {
        gchar* msg = g_strconcat(_("Error writing config file %s"), m_config_filename, NULL);
        show_error(this, msg);
        g_free(msg);
        g_error_free(err);
        goto finish;
    }

finish:

    g_key_file_free(config);
    g_free(m_config_filename);

    while (!clientNames.empty()) {
        std::map<uint32_t, char*>::iterator i = clientNames.begin();
        g_free(i->second);
        clientNames.erase(i);
    }
}

void MainWindow::selectTab(int tab_number) {
    AdwViewStack *stack =  ADW_VIEW_STACK(viewStack->gobj());
    switch (tab_number) {
    case 1 :
        adw_view_stack_set_visible_child_name(stack, "playback");
        break;
    case 2 :
        adw_view_stack_set_visible_child_name(stack, "recording");
        break;
    case 3 :
        adw_view_stack_set_visible_child_name(stack, "sinks");
        break;
    case 4 :
        adw_view_stack_set_visible_child_name(stack, "sources");
        break;
    case 5 :
        adw_view_stack_set_visible_child_name(stack, "configuration");
        break;
    }
}

void MainWindow::updateCard(const pa_card_info &info) {
    configFrame->updateCard(info);

    gboolean hasSinks = false, hasSources = false;
    for (uint32_t i=0; i<info.n_profiles; ++i) {
        hasSinks = hasSinks || (info.profiles[i].n_sinks > 0);
        hasSources = hasSources || (info.profiles[i].n_sources > 0);
    }
    if (hasSinks) {
       sinkFrame->updatePortsLatencyOffsetForCard(info.index, configFrame->findPortLatencyOffsetForCard(info.index));
    }
    if (hasSources) {
       sourceFrame->updatePortsLatencyOffsetForCard(info.index, configFrame->findPortLatencyOffsetForCard(info.index));
    }
}

void MainWindow::updateCardCodecs(const std::string& card_name, const std::unordered_map<std::string, std::string>& codecs) {
    configFrame->updateCardCodecs(card_name, codecs);
}

void MainWindow::setActiveCodec(const std::string& card_name, const std::string& codec) {
    configFrame->setActiveCodec(card_name, codec);
}

void MainWindow::setCardProfileIsSticky(const std::string& card_name, gboolean profile_is_sticky) {
    configFrame->setCardProfileIsSticky(card_name, profile_is_sticky);
}

bool MainWindow::updateSink(const pa_sink_info &info) {
    bool is_new = sinkFrame->updateSink(info);
    sinkInputFrame->updateAvailableSinks(sinkFrame->makeDeviceComboBox());
    auto sink_inputs_without_monitor = sink_inputs_without_monitor_by_sink[info.index];
    for (auto sink_input = sink_inputs_without_monitor.begin() ; sink_input != sink_inputs_without_monitor.end(); sink_input++) {
        createMonitorStreamForSinkInput(*sink_input, info.index);
    }
    return is_new;
}

static void suspended_callback(pa_stream *s, void *userdata) {
    MainWindow *w = static_cast<MainWindow*>(userdata);

    if (pa_stream_is_suspended(s))
        w->updateVolumeMeter(pa_stream_get_device_index(s), PA_INVALID_INDEX, -1);
}

static void read_callback(pa_stream *s, size_t length, void *userdata) {
    MainWindow *w = static_cast<MainWindow*>(userdata);
    const void *data;
    double v;

    if (pa_stream_peek(s, &data, &length) < 0) {
        show_error(w, _("Failed to read data from stream"));
        return;
    }

    if (!data) {
        /* NULL data means either a hole or empty buffer.
         * Only drop the stream when there is a hole (length > 0) */
        if (length)
            pa_stream_drop(s);
        return;
    }

    assert(length > 0);
    assert(length % sizeof(float) == 0);

    v = ((const float*) data)[length / sizeof(float) -1];

    pa_stream_drop(s);

    if (v < 0)
        v = 0;
    if (v > 1)
        v = 1;

    w->updateVolumeMeter(pa_stream_get_device_index(s), pa_stream_get_monitor_stream(s), v);
}

pa_stream* MainWindow::createMonitorStream(uint32_t source_idx, uint32_t stream_idx, bool suspend = false) {
    pa_stream *s;
    char t[16];
    pa_buffer_attr attr;
    pa_sample_spec ss;
    pa_stream_flags_t flags;

    ss.channels = 1;
    ss.format = PA_SAMPLE_FLOAT32;
    ss.rate = PEAKS_RATE;

    memset(&attr, 0, sizeof(attr));
    attr.fragsize = sizeof(float);
    attr.maxlength = (uint32_t) -1;

    snprintf(t, sizeof(t), "%u", source_idx);

    if (!(s = pa_stream_new(get_context(), _("Peak detect"), &ss, NULL))) {
        show_error(this, _("Failed to create monitoring stream"));
        return NULL;
    }

    if (stream_idx != (uint32_t) -1)
        pa_stream_set_monitor_stream(s, stream_idx);

    pa_stream_set_read_callback(s, read_callback, this);
    pa_stream_set_suspended_callback(s, suspended_callback, this);

    flags = (pa_stream_flags_t) (PA_STREAM_DONT_MOVE | PA_STREAM_PEAK_DETECT | PA_STREAM_ADJUST_LATENCY |
                                 (suspend ? PA_STREAM_DONT_INHIBIT_AUTO_SUSPEND : PA_STREAM_NOFLAGS));

    if (pa_stream_connect_record(s, t, &attr, flags) < 0) {
        show_error(this, _("Failed to connect monitoring stream"));
        pa_stream_unref(s);
        return NULL;
    }
    return s;
}

void MainWindow::createMonitorStreamForSource(uint32_t source_idx, bool suspend) {
    if (!sourcePeaks.count(source_idx)){
        sourcePeaks[source_idx] = createMonitorStream(source_idx, -1, suspend);
    }
}

void MainWindow::createMonitorStreamForSinkInput(uint32_t sink_input_idx, uint32_t sink_idx) {
    uint32_t monitor_idx = sinkFrame->getMonitorForSink(sink_idx);
    if (monitor_idx == (uint32_t)-1) {
        sink_inputs_without_monitor_by_sink[sink_idx].push_back(sink_input_idx);
        if (sinkInputPeaks.count(sink_input_idx)){
            pa_stream_disconnect(sinkInputPeaks[sink_input_idx].second);
            pa_stream_unref(sinkInputPeaks[sink_input_idx].second);
            sinkInputPeaks.erase(sink_input_idx);
        }

        return;
    }
    if (!sinkInputPeaks.count(sink_input_idx)){
        sinkInputPeaks[sink_input_idx] = {sink_idx , createMonitorStream(monitor_idx, sink_input_idx)};
    }
    else {
        auto stored = sinkInputPeaks[sink_input_idx];
        auto previous_sink_id = stored.first;
        auto previous_peak = stored.second;
        if (previous_sink_id != sink_idx) {
            if (previous_peak != NULL) {
                pa_stream_disconnect(previous_peak);
                pa_stream_unref(previous_peak);
            }
            sinkInputPeaks[sink_input_idx] = {sink_idx , createMonitorStream(monitor_idx, sink_input_idx)};
        }
    }
}

void MainWindow::updateSource(const pa_source_info &info) {
    sourceFrame->updateSource(info);
    sourceOutputFrame->updateAvailableSources(sourceFrame->makeDeviceComboBox());
    if (pa_context_get_server_protocol_version(get_context()) >= 13)
        createMonitorStreamForSource(info.index, !!(info.flags & PA_SOURCE_NETWORK));
}

void MainWindow::updateSinkInput(const pa_sink_input_info &info) {
    char* client_name = NULL;
    if (clientNames.count(info.client))
        client_name = clientNames[info.client];

    bool require_monitor = sinkInputFrame->updateSinkInput(info, client_name);
    if (require_monitor)
        createMonitorStreamForSinkInput(info.index, info.sink);
}

void MainWindow::updateSourceOutput(const pa_source_output_info &info) {
    char* client_name = NULL;
    if (clientNames.count(info.client))
        client_name = clientNames[info.client];
    sourceOutputFrame->updateSourceOutput(info, client_name);
}

void MainWindow::updateClient(const pa_client_info &info) {
    g_free(clientNames[info.index]);
    clientNames[info.index] = g_strdup(info.name);

    sinkInputFrame->updateClient(info);
    sourceOutputFrame->updateClient(info);
}

void MainWindow::updateServer(const pa_server_info &info) {
    sinkFrame->updateServer(info);
    sourceFrame->updateServer(info);
}

void MainWindow::deleteEventRoleRow() {
    sinkInputFrame->deleteEventRoleRow();
}

void MainWindow::updateRole(const pa_ext_stream_restore_info &info) {
    sinkInputFrame->updateRole(info);
}

#if HAVE_EXT_DEVICE_RESTORE_API
void MainWindow::updateDeviceInfo(const pa_ext_device_restore_info &info) {
    sinkFrame->updateDeviceInfo(info);
}
#endif


void MainWindow::updateVolumeMeter(uint32_t source_index, uint32_t sink_input_idx, double v) {
    if (sink_input_idx != PA_INVALID_INDEX) {
        sinkInputFrame->updateVolumeMeter(sink_input_idx, v);

    } else {
        sinkFrame->updateVolumeMeter(source_index, v);
        sourceFrame->updateVolumeMeter(source_index, v);
        sourceOutputFrame->updateVolumeMeter(source_index, v);
    }
}

void load_module_cb(pa_context *c, uint32_t index, void *userdata) {
    MainWindow *w = static_cast<MainWindow*>(userdata);
    if (index != (uint32_t)-1 ) {
        w->setModuleDeviceManagerIndex(index);
    }
}

void MainWindow::setConnectionState(gboolean connected) {
    if (m_connected != connected) {
        m_connected = connected;
        if (m_connected) {
            if (moduleDeviceManagerIndex == (uint32_t)-1) {
                pa_operation *o;
                if (!(o = pa_context_load_module(get_context(), "module-device-manager", "",load_module_cb, this))) {
                    show_error(this, _("pa_context_get_sink_info_by_index() failed"));
                }
                pa_operation_unref(o);
            }
            connectingLabel->hide();
            viewStack->show();
        } else {
            viewStack->hide();
            connectingLabel->show();
        }
    }
}


void MainWindow::removeCard(uint32_t index) {
    configFrame->removeCard(index);
}

void MainWindow::removeSink(uint32_t index) {
    sinkFrame->removeSink(index);
    sinkInputFrame->updateAvailableSinks(sinkFrame->makeDeviceComboBox());
}

void MainWindow::removeSource(uint32_t index) {
    sourceFrame->removeSource(index);
    sourceOutputFrame->updateAvailableSources(sourceFrame->makeDeviceComboBox());
    if (sourcePeaks[index] != NULL) {
        pa_stream_disconnect(sourcePeaks[index]);
        pa_stream_unref(sourcePeaks[index]);
        sourcePeaks.erase(index);
    }
}

void MainWindow::removeSinkInput(uint32_t index) {
    sinkInputFrame->removeSinkInput(index);

    if (sinkInputPeaks[index].second != NULL) {
        pa_stream_disconnect(sinkInputPeaks[index].second);
        pa_stream_unref(sinkInputPeaks[index].second);
        sinkInputPeaks.erase(index);
    }
}

void MainWindow::removeSourceOutput(uint32_t index) {
    sourceOutputFrame->removeSourceOutput(index);
}

void MainWindow::removeClient(uint32_t index) {
    g_free(clientNames[index]);
    clientNames.erase(index);
}

void MainWindow::removeAllWidgets() {
    sinkInputFrame->removeAllSinkInputs();
    sourceOutputFrame->removeAllSourceOutput();
    sinkFrame->removeAllSinks();
    sourceFrame->removeAllSources();
    configFrame->removeAllCards();
    while (!clientNames.empty())
        removeClient(clientNames.begin()->first);
    deleteEventRoleRow();
}

void MainWindow::setConnectingMessage(const char *string) {
    Glib::ustring markup = "<i>";
    if (!string)
        markup += _("Establishing connection to PulseAudio. Please wait...");
    else
        markup += string;
    markup += "</i>";
    connectingLabel->set_markup(markup);
}


void MainWindow::removedModule(uint32_t index) {
    if (index == moduleDeviceManagerIndex) {
        moduleDeviceManagerIndex = -1;
        sinkFrame->setCanRenameDevices(false);
        sourceFrame->setCanRenameDevices(false);
    }
}


void MainWindow::setModuleDeviceManagerIndex(uint32_t index) {
    moduleDeviceManagerIndex = index;
    sinkFrame->setCanRenameDevices(true);
    sourceFrame->setCanRenameDevices(true);
}

