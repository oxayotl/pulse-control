/***
  This file is part of pulsecontrol.

  Copyright 2006-2008 Lennart Poettering
  Copyright 2009 Colin Guthrie

  pulsecontrol is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  pulsecontrol is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pulsecontrol. If not, see <http://www.gnu.org/licenses/>.
***/

#include "stream-row.h"
#include "mainwindow.h"
#include "widgets/channelwidget.h"
#include "widgets/pulsecontroldropdown.h"
#include "i18n.h"

/*** StreamRow ***/
StreamRow::StreamRow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x) :
    VolumeRow(cobject, x) {


    directionLabel = x->get_widget<Gtk::Label>("directionLabel");
    deviceComboBox = Gtk::Builder::get_widget_derived<PulseControlDropDown>(x, "deviceComboBox");

    muteToggleButton->signal_clicked().connect(sigc::mem_fun(*this, &StreamRow::onMuteToggleButton));
    lockToggleButton->signal_clicked().connect(sigc::mem_fun(*this, &StreamRow::onLockToggleButton));
    deviceComboBox->connect_property_changed("selected", sigc::mem_fun(*this, &StreamRow::onDeviceComboBoxChanged));

    for (unsigned i = 0; i < PA_CHANNELS_MAX; i++)
        channelWidgets[i] = NULL;
}

void StreamRow::setChannelMap(const pa_channel_map &m, bool can_decibel) {
    VolumeRow::setChannelMap(m, can_decibel);
    channelWidgets[m.channels-1]->setBaseVolume(PA_VOLUME_NORM);
}

void StreamRow::onDeviceComboBoxChanged() {
}

void StreamRow::setIcon(const pa_proplist *props) {
    const char *t;
    Glib::RefPtr<Gtk::IconTheme> theme = Gtk::IconTheme::get_for_display(Gdk::Display::get_default());

    if ((t = pa_proplist_gets(props, PA_PROP_MEDIA_ICON_NAME)) && theme->has_icon(t)) {
        iconImage->set_from_icon_name(t);
        return;
    }

    if ((t = pa_proplist_gets(props, PA_PROP_WINDOW_ICON_NAME)) && theme->has_icon(t)) {
        iconImage->set_from_icon_name(t);
        return;
    }

    if ((t = pa_proplist_gets(props, PA_PROP_APPLICATION_ICON_NAME)) && theme->has_icon(t)){
        iconImage->set_from_icon_name(t);
        return;
    }

    if ((t = pa_proplist_gets(props, PA_PROP_MEDIA_ROLE))) {

        if (strcmp(t, "video") == 0 || strcmp(t, "phone") == 0) {
            iconImage->set_from_icon_name(t);
            return;
        }

        if (strcmp(t, "music") == 0) {
            iconImage->set_from_icon_name("audio");
            return;
        }

        if (strcmp(t, "game") == 0) {
            iconImage->set_from_icon_name("applications-games");
            return;
        }

        if (strcmp(t, "event") == 0) {
            iconImage->set_from_icon_name("dialog-information");
            return;
        }
    }
    iconImage->set_from_icon_name("application-x-executable-symbolic");
}

