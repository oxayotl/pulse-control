/***
  This file is part of pulsecontrol.

  Copyright 2006-2008 Lennart Poettering
  Copyright 2009 Colin Guthrie

  pulsecontrol is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  pulsecontrol is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pulsecontrol. If not, see <http://www.gnu.org/licenses/>.
***/

#include <pulse/ext-device-manager.h>

#include "mainwindow.h"
#include "device-row.h"
#include "widgets/channelwidget.h"
#include "widgets/pulsecontroldropdown.h"

#include "i18n.h"

/*** DeviceRow ***/
DeviceRow::DeviceRow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x, Glib::ustring deviceType, bool canRenameWidget, const char* default_icon) :
    VolumeRow(cobject, x),
    activePort(""),
    offsetButtonEnabled(false),
    mDigital(false),
    icon_name(NULL),
    default_icon_name(default_icon),
    icon_theme(Gtk::IconTheme::get_for_display(Gdk::Display::get_default())),
    mDeviceType(deviceType) {
    deviceNameEntry = x->get_widget<Gtk::Entry>("deviceNameEntry");
    deviceNameEntry->signal_activate().connect(sigc::mem_fun(*this, &DeviceRow::rename));
    deviceNameButton = x->get_widget<Gtk::Button>("deviceNameButton");
    deviceNameButton->signal_clicked().connect(sigc::mem_fun(*this, &DeviceRow::showRenameEntry));

    auto event_controller_key = Gtk::EventControllerKey::create();
    event_controller_key->signal_key_pressed().connect(sigc::mem_fun(*this, &DeviceRow::on_rename_entry_key_press_event), false);
    deviceNameEntry->add_controller(event_controller_key);

    defaultToggleButton= x->get_widget<Gtk::ToggleButton>("defaultToggleButton");
    portSelect = x->get_widget<Gtk::Box>("portSelect");
    portList = Gtk::Builder::get_widget_derived<PulseControlDropDown>(x, "portList");
    advancedOptions = x->get_widget<Gtk::Expander>("advancedOptions");
    offsetSelect = x->get_widget<Gtk::Box>("offsetSelect");
    offsetButton = x->get_widget<Gtk::SpinButton>("offsetButton");

    muteToggleButton->signal_clicked().connect(sigc::mem_fun(*this, &DeviceRow::onMuteToggleButton));
    lockToggleButton->signal_clicked().connect(sigc::mem_fun(*this, &DeviceRow::onLockToggleButton));
    defaultToggleButton->signal_clicked().connect(sigc::mem_fun(*this, &DeviceRow::onDefaultToggleButton));


    portList->connect_property_changed("selected", sigc::mem_fun(*this, &DeviceRow::onPortChange));
    offsetButton->signal_value_changed().connect(sigc::mem_fun(*this, &DeviceRow::onOffsetChange));

    for (unsigned i = 0; i < PA_CHANNELS_MAX; i++)
        channelWidgets[i] = NULL;

    offsetAdjustment = Gtk::Adjustment::create(0.0, -2000.0, 5000.0, 10.0, 50.0, 0.0);
    offsetButton->configure(offsetAdjustment, 0, 2);
    canRenameDevice(canRenameWidget);

    icon_theme->signal_changed().connect(sigc::mem_fun(*this, &DeviceRow::setIcon));
}

void DeviceRow::canRenameDevice(bool canRenameDevice) {
    deviceNameButton->set_sensitive(canRenameDevice);
    deviceNameButton->set_has_tooltip(!canRenameDevice);
}

bool DeviceRow::on_rename_entry_key_press_event(guint keyval, guint keycode, Gdk::ModifierType state) {
    switch (keyval) {
        case GDK_KEY_Escape:
            hideRenameEntry();
            return true;
    }
    return false;
}

void DeviceRow::showRenameEntry() {
    deviceNameEntry->set_text(nameLabel->get_text());
    nameLabel->set_visible(false);
    deviceNameButton->set_visible(false);
    deviceNameEntry->set_visible(true);
    deviceNameEntry->grab_focus();
}

void DeviceRow::hideRenameEntry() {
    nameLabel->set_visible(true);
    deviceNameButton->set_visible(true);
    deviceNameEntry->set_visible(false);
}


void DeviceRow::onOffsetChange() {
    pa_operation *o;
    int64_t offset;
    std::ostringstream card_stream;
    Glib::ustring card_name;

    if (!offsetButtonEnabled)
        return;

    offset = offsetButton->get_value() * 1000.0;
    card_stream << card_index;
    card_name = card_stream.str();

    if (!(o = pa_context_set_port_latency_offset(get_context(),
            card_name.c_str(), activePort.c_str(), offset, NULL, NULL))) {
        show_error(this, _("pa_context_set_port_latency_offset() failed"));
        return;
    }
    pa_operation_unref(o);
}

void DeviceRow::setIsDefault(Glib::ustring defaultDeviceName) {
    disable_callback = true;
    defaultToggleButton->set_active(name == defaultDeviceName);
    disable_callback = false;
    /*defaultToggleButton->set_sensitive(name != defaultDeviceName);*/
}

void DeviceRow::setLatencyOffset(int64_t offset) {
    offsetButtonEnabled = false;
    offsetButton->set_value(offset / 1000.0);
    offsetButtonEnabled = true;
}

void DeviceRow::setBaseVolume(pa_volume_t v) {
    for (int i = 0; i < channelMap.channels; i++)
        channelWidgets[i]->setBaseVolume(v);
}

void DeviceRow::rename(){
    pa_operation* o;
    gchar *key = g_markup_printf_escaped("%s:%s", mDeviceType.c_str(), name.c_str());
    auto name = deviceNameEntry->get_text();
    if (name.empty())
        return;


    if (!(o = pa_ext_device_manager_set_device_description(get_context(), key, name.c_str(), NULL, NULL))) {
        show_error(this, _("pa_ext_device_manager_write() failed"));
        return;
    }
    pa_operation_unref(o);

    hideRenameEntry();
}

void DeviceRow::updateAdvancedOptionsVisibility() {
    bool visible = false;

    if (mDigital) {
        /* We need to show the format configuration options. */
        visible = true;
    }

    if (offsetSelect->is_visible()) {
        /* We need to show the latency offset spin button. */
        visible = true;
    }

    if (visible)
        advancedOptions->show();
    else
        advancedOptions->hide();
}

void DeviceRow::setIcon() {
    char* icon = (icon_name != NULL) ? strdup(icon_name) : strdup("");
    std::string iconName(icon);

    while (!icon_theme->has_icon((iconName + "-symbolic").c_str())) {
        size_t lastDashIndex = iconName.find_last_of("-");
        if (lastDashIndex == std::string::npos) {
            g_free(icon);
            std::string defaultIcon(default_icon_name);
            icon = strdup((defaultIcon + "-symbolic").c_str());
            break;
        }
        iconName = iconName.substr(0,lastDashIndex);
        g_free(icon);
        icon = strdup((iconName + "-symbolic").c_str());
    }
    iconImage->set_from_icon_name(icon);
    g_free(icon);
}

DeviceComboBoxItem* DeviceRow::makeDeviceComboBoxItem() {
    return new DeviceComboBoxItem{name, description, index, 0, hasActivePort};
}


