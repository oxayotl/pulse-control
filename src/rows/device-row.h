/***
  This file is part of pulsecontrol.

  Copyright 2006-2008 Lennart Poettering
  Copyright 2009 Colin Guthrie

  pulsecontrol is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  pulsecontrol is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pulsecontrol. If not, see <http://www.gnu.org/licenses/>.
***/

#ifndef device_row_h
#define device_row_h

#include "pulsecontrol.h"

#include "volume-row.h"

class MainWindow;
class PulseControlDropDown;

class DeviceRow : public VolumeRow {
public:

    Glib::ustring name;
    Glib::ustring description;
    uint32_t index, card_index;

    virtual void setIsDefault(Glib::ustring defaultDeviceName);
    void canRenameDevice(bool value);

    DeviceComboBoxItem* makeDeviceComboBoxItem();

    Glib::ustring activePort;

    void setLatencyOffset(int64_t offset);
protected:
    DeviceRow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x, Glib::ustring deviceType, bool canRenameWidget, const char* default_icon);

    virtual void setBaseVolume(pa_volume_t v);
    void openRenamePopup(const Glib::VariantBase& parameter);
    /* Shows or hides the advanced options expander depending on whether it's
     * useful or not. This is called always after ports or mDigital have been
     * updated. */
    void updateAdvancedOptionsVisibility();

    virtual void onPortChange() = 0;

    gboolean hasActivePort;

    void onOffsetChange();

    bool offsetButtonEnabled;

    Gtk::Expander *advancedOptions;
    Gtk::Box *portSelect, *offsetSelect;
    PulseControlDropDown *portList;
    Glib::RefPtr<Gtk::ListStore> treeModel;
    Glib::RefPtr<Gtk::Adjustment> offsetAdjustment;

    Gtk::Entry *deviceNameEntry;
    Gtk::Button *deviceNameButton;

    Gtk::ToggleButton *defaultToggleButton;
    Gtk::SpinButton *offsetButton;


    virtual void onDefaultToggleButton() = 0;
    bool on_rename_entry_key_press_event(guint keyval, guint keycode, Gdk::ModifierType state);
    void showRenameEntry();
    void hideRenameEntry();
    void rename();
    /* Set to true for "digital" sinks (in practice this means those sinks that
     * support format configuration). */
    bool mDigital;

    const char* icon_name;
    const char* default_icon_name;
    Glib::RefPtr<Gtk::IconTheme> icon_theme;
    void setIcon();

private:
    Glib::ustring mDeviceType;
};

#endif

