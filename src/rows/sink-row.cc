/***
  This file is part of pulsecontrol.

  Copyright 2006-2008 Lennart Poettering
  Copyright 2009 Colin Guthrie

  pulsecontrol is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  pulsecontrol is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pulsecontrol. If not, see <http://www.gnu.org/licenses/>.
***/

#include "sink-row.h"
#include "widgets/containerwidget.h"
#include "widgets/pulsecontroldropdown.h"

#include <canberra.h>
#if HAVE_EXT_DEVICE_RESTORE_API
#  include <pulse/format.h>
#  include <pulse/ext-device-restore.h>
#endif

#include "i18n.h"

SinkRow::SinkRow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x, const pa_sink_info &info, bool canRenameWidget) :
    DeviceRow(cobject, x, "sink", canRenameWidget, "audio-card") {
    ca_context_create (&canberraContext);
    ca_context_set_driver(canberraContext, "pulse");

    initializeEncodingWidgets(x);

    index = info.index;
    monitor_index = info.monitor_source;
    setChannelMap(info.channel_map, !!(info.flags & PA_SINK_DECIBEL_VOLUME));
    setBaseVolume(info.base_volume);

    owner_module_index = info.owner_module;
    if (!(info.flags & PA_SINK_HARDWARE) && owner_module_index != PA_INVALID_INDEX)
        addKillButton(_("Remove sink"));

}

SinkRow::~SinkRow(){
    ca_context_destroy(canberraContext);
}

bool SinkRow::hasActiveSink(const pa_sink_info &info) {
    for (uint32_t i=0; i<info.n_ports; ++i) {
        if( info.ports[i]->available == PA_PORT_AVAILABLE_YES
        || info.ports[i]->available == PA_PORT_AVAILABLE_UNKNOWN) {
            return true;
        }
    }
    hasActivePort = info.n_ports == 0;
    return hasActivePort;
}

SinkRow* SinkRow::create(ContainerWidget *container, bool canRenameWidget, const pa_sink_info &info) {
    SinkRow* w;
    Glib::RefPtr<Gtk::Builder> x = Gtk::Builder::create_from_resource("/pulsecontrol/ui/rows/device-row.ui");
    w = Gtk::Builder::get_widget_derived<SinkRow>(x, "deviceWidget", info, canRenameWidget);

    if (w->hasActiveSink(info)) {
        container->addWidget(w);
    }
    else {
        container->addHiddenWidget(w);
    }
    w->container = container;

    return w;
}

void SinkRow::updateSink(const pa_sink_info &info) {
    card_index = info.card;
    name = info.name;
    description = info.description;
    type = info.flags & PA_SINK_HARDWARE ? SINK_HARDWARE : SINK_VIRTUAL;

    boldNameLabel->set_text("");
    gchar *txt;
    nameLabel->set_markup(txt = g_markup_printf_escaped("%s", info.description));
    nameLabel->set_tooltip_text(info.description);
    g_free(txt);

    if (icon_name == NULL || strcmp (pa_proplist_gets(info.proplist, PA_PROP_DEVICE_ICON_NAME), icon_name) != 0) {
        if (pa_proplist_gets(info.proplist, PA_PROP_DEVICE_ICON_NAME) != NULL)
            icon_name = strdup(pa_proplist_gets(info.proplist, PA_PROP_DEVICE_ICON_NAME));
        setIcon();
    }

    setVolume(info.volume);
    disable_callback = true;
    muteToggleButton->set_active(info.mute);
    disable_callback = false;

    activePort = info.active_port ? info.active_port->name : "";

    disable_callback = true;
    portList->remove_all();
    if (info.ports != NULL)
    {
        for (uint32_t i = 0; i < info.n_ports; i++) {
            pa_sink_port_info *p = info.ports[i];
            Glib::ustring name = p->name;
            Glib::ustring desc = p->description;

            if (p->available == PA_PORT_AVAILABLE_YES)
                desc +=  _(" (plugged in)");
            else if (p->available == PA_PORT_AVAILABLE_NO) {
                if (name == "analog-output-speaker" ||
                    name == "analog-input-microphone-internal")
                    desc += _(" (unavailable)");
                else
                    desc += _(" (unplugged)");
            }
            bool available = (p->available == PA_PORT_AVAILABLE_YES) || (p->available == PA_PORT_AVAILABLE_UNKNOWN);
            portList->append(p->name, desc, p->priority, available);
        }
        portList->set_active_id(activePort);
    }
    disable_callback = false;

    if (info.n_ports > 0) {
        portSelect->show();

        if (pa_context_get_server_protocol_version(get_context()) >= 27)
            offsetSelect->show();
        else
            offsetSelect->hide();

    } else {
        portSelect->hide();
        offsetSelect->hide();
    }

    updateAdvancedOptionsVisibility();

#ifdef PA_SINK_SET_FORMATS
    setDigital(info.flags & PA_SINK_SET_FORMATS);
#endif

    updateAdvancedOptionsVisibility();

    container->updateHiddenStatus(this, !hasActiveSink(info));
}

#if HAVE_EXT_DEVICE_RESTORE_API
void SinkRow::updateDeviceInfo(const pa_ext_device_restore_info &info) {
    pa_format_info *format;

    disable_callback = true;

    /* Unselect everything */
    for (int j = 1; j < PAVU_NUM_ENCODINGS; ++j)
        encodings[j].widget->set_active(false);


    for (uint8_t i = 0; i < info.n_formats; ++i) {
        format = info.formats[i];
        for (int j = 1; j < PAVU_NUM_ENCODINGS; ++j) {
            if (format->encoding == encodings[j].encoding) {
                encodings[j].widget->set_active(true);
                break;
            }
        }
    }

    disable_callback = false;
}
#endif

void SinkRow::setDigital(bool digital) {
    mDigital = digital;

#if HAVE_EXT_DEVICE_RESTORE_API
    if (digital)
        encodingSelect->show();
    else
        encodingSelect->hide();

    updateAdvancedOptionsVisibility();
#endif
}

void SinkRow::executeVolumeUpdate() {
    pa_operation* o;
    int playing = 0;

    if (!(o = pa_context_set_sink_volume_by_index(get_context(), index, &volume, NULL, NULL))) {
        show_error(this, _("pa_context_set_sink_volume_by_index() failed"));
        return;
    }

    pa_operation_unref(o);
    char dev[64];
    snprintf(dev, sizeof(dev), "%lu", (unsigned long) index);
    ca_context_change_device(canberraContext, dev);

    ca_context_playing(canberraContext, 2, &playing);
    if (playing)
        return;

    ca_context_play(canberraContext,
                     2,
                     CA_PROP_EVENT_DESCRIPTION, _("Volume Control Feedback Sound"),
                     CA_PROP_EVENT_ID, "audio-volume-change",
                     CA_PROP_CANBERRA_CACHE_CONTROL, "permanent",
                     CA_PROP_CANBERRA_ENABLE, "1",
                     NULL);
}

void SinkRow::onMuteToggleButton() {
    DeviceRow::onMuteToggleButton();

    if (disable_callback)
        return;

    pa_operation* o;
    if (!(o = pa_context_set_sink_mute_by_index(get_context(), index, muteToggleButton->get_active(), NULL, NULL))) {
        show_error(this, _("pa_context_set_sink_mute_by_index() failed"));
        return;
    }

    pa_operation_unref(o);
}

void SinkRow::onDefaultToggleButton() {

    if (disable_callback)
        return;

    pa_operation* o;
    if (!(o = pa_context_set_default_sink(get_context(), name.c_str(), NULL, NULL))) {
        show_error(this, _("pa_context_set_default_sink() failed"));
        return;
    }
    pa_operation_unref(o);
}

void SinkRow::onKill() {
    pa_operation* o;
    if (!(o = pa_context_unload_module(get_context(), owner_module_index, NULL, NULL))) {
        show_error(this, _("pa_context_unload_module() failed"));
        return;
    }

    pa_operation_unref(o);
}

void SinkRow::onPortChange() {
    if (disable_callback)
        return;

    pa_operation* o;
    Glib::ustring port = portList->get_active_id();

    if (!(o = pa_context_set_sink_port_by_index(get_context(), index, port.c_str(), NULL, NULL))) {
        show_error(this, _("pa_context_set_sink_port_by_index() failed"));
        return;
    }

    pa_operation_unref(o);
}

void SinkRow::onEncodingsChange() {
#if HAVE_EXT_DEVICE_RESTORE_API
    pa_operation* o;
    uint8_t n_formats = 0;
    pa_format_info **formats;

    if (disable_callback)
        return;

    formats = (pa_format_info**)malloc(sizeof(pa_format_info*) * PAVU_NUM_ENCODINGS);

    for (int i = 0; i < PAVU_NUM_ENCODINGS; ++i) {
        if (encodings[i].widget->get_active()) {
            formats[n_formats] = pa_format_info_new();
            formats[n_formats]->encoding = encodings[i].encoding;
            ++n_formats;
        }
    }

    if (!(o = pa_ext_device_restore_save_formats(get_context(), PA_DEVICE_TYPE_SINK, index, n_formats, formats, NULL, NULL))) {
        show_error(this, _("pa_ext_device_restore_save_sink_formats() failed"));
        free(formats);
        return;
    }

    free(formats);
    pa_operation_unref(o);
#endif
}

void SinkRow::initializeEncodingWidgets(const Glib::RefPtr<Gtk::Builder>& x) {
#if HAVE_EXT_DEVICE_RESTORE_API
    uint8_t i = 0;

    encodingSelect = x->get_widget<Gtk::Grid>("encodingSelect");

    encodings[i].encoding = PA_ENCODING_PCM;
    encodings[i].widget = x->get_widget<Gtk::CheckButton>("encodingFormatPCM");
    encodings[i].widget->signal_toggled().connect(sigc::mem_fun(*this, &SinkRow::onEncodingsChange));

    ++i;
    encodings[i].encoding = PA_ENCODING_AC3_IEC61937;
    encodings[i].widget = x->get_widget<Gtk::CheckButton>("encodingFormatAC3");
    encodings[i].widget->signal_toggled().connect(sigc::mem_fun(*this, &SinkRow::onEncodingsChange));

    ++i;
    encodings[i].encoding = PA_ENCODING_EAC3_IEC61937;
    encodings[i].widget = x->get_widget<Gtk::CheckButton>("encodingFormatEAC3");
    encodings[i].widget->signal_toggled().connect(sigc::mem_fun(*this, &SinkRow::onEncodingsChange));

    ++i;
    encodings[i].encoding = PA_ENCODING_MPEG_IEC61937;
    encodings[i].widget = x->get_widget<Gtk::CheckButton>("encodingFormatMPEG");
    encodings[i].widget->signal_toggled().connect(sigc::mem_fun(*this, &SinkRow::onEncodingsChange));

    ++i;
    encodings[i].encoding = PA_ENCODING_DTS_IEC61937;
    encodings[i].widget = x->get_widget<Gtk::CheckButton>("encodingFormatDTS");
    encodings[i].widget->signal_toggled().connect(sigc::mem_fun(*this, &SinkRow::onEncodingsChange));

    ++i;
    encodings[i].encoding = PA_ENCODING_INVALID;
    encodings[i].widget = x->get_widget<Gtk::CheckButton>("encodingFormatAAC");
    encodings[i].widget->set_sensitive(false);
#ifdef PA_ENCODING_MPEG2_AAC_IEC61937
    if (pa_context_get_server_protocol_version(get_context()) >= 28) {
        encodings[i].encoding = PA_ENCODING_MPEG2_AAC_IEC61937;
        encodings[i].widget->signal_toggled().connect(sigc::mem_fun(*this, &SinkRow::onEncodingsChange));
        encodings[i].widget->set_sensitive(true);
    }
#endif
    ++i;
    encodings[i].encoding = PA_ENCODING_INVALID;
    encodings[i].widget = x->get_widget<Gtk::CheckButton>("encodingFormatTRUEHD");
    encodings[i].widget->set_sensitive(false);
#ifdef PA_ENCODING_TRUEHD_IEC61937
    if (pa_context_get_server_protocol_version(get_context()) >= 33) {
        encodings[i].encoding = PA_ENCODING_TRUEHD_IEC61937;
        encodings[i].widget->signal_toggled().connect(sigc::mem_fun(*this, &SinkRow::onEncodingsChange));
        encodings[i].widget->set_sensitive(true);
    }
#endif
    ++i;
    encodings[i].encoding = PA_ENCODING_INVALID;
    encodings[i].widget = x->get_widget<Gtk::CheckButton>("encodingFormatDTSHD");
    encodings[i].widget->set_sensitive(false);
#ifdef PA_ENCODING_DTSHD_IEC61937
    if (pa_context_get_server_protocol_version(get_context()) >= 33) {
        encodings[i].encoding = PA_ENCODING_DTSHD_IEC61937;
        encodings[i].widget->signal_toggled().connect(sigc::mem_fun(*this, &SinkRow::onEncodingsChange));
        encodings[i].widget->set_sensitive(true);
    }
#endif
#endif
};
