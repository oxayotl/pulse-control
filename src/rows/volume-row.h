/***
  This file is part of pulsecontrol.

  Copyright 2006-2008 Lennart Poettering
  Copyright 2009 Colin Guthrie

  pulsecontrol is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  pulsecontrol is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pulsecontrol. If not, see <http://www.gnu.org/licenses/>.
***/

#ifndef volume_row_h
#define volume_row_h

#include "pulsecontrol.h"

class ChannelWidget;

class VolumeRow : public Gtk::ListBoxRow {
public:
    VolumeRow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x);
    virtual ~VolumeRow();


    void setVolume(const pa_cvolume &volume, bool force = false);
    virtual void updateChannelVolume(int channel, pa_volume_t v);

    void updatePeak(double v);

    ContainerWidget *container;

protected:
    Gtk::Image *iconImage;
    Gtk::Label *nameLabel, *boldNameLabel;
    Gtk::Box *channelsBox;
    ChannelWidget *channelWidgets[PA_CHANNELS_MAX];
    Gtk::ToggleButton *lockToggleButton, *muteToggleButton;
    Gtk::LevelBar *peakLevelBar;
    Gtk::Button *killStreamButton;
    void addKillButton(const char* killLabel);


    double lastPeak;
    bool volumeMeterEnabled;
    void enableVolumeMeter();

    sigc::connection timeoutConnection;
    bool timeoutEvent();

    pa_channel_map channelMap;
    pa_cvolume volume;

    void hideLockedChannels(bool hide = true);
    void setChannelMap(const pa_channel_map &m, bool can_decibel);

    virtual void onMuteToggleButton();
    virtual void onLockToggleButton();
    virtual void executeVolumeUpdate() = 0;
    virtual void onKill();

    // Disactivate callbacks since changes in buttons states, comboxbox selected items etc
    // aren't new user commands but status change reflection
    bool disable_callback;
};

#endif

