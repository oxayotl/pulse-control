/***
  This file is part of pulsecontrol.

  Copyright 2006-2008 Lennart Poettering
  Copyright 2009 Colin Guthrie

  pulsecontrol is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  pulsecontrol is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pulsecontrol. If not, see <http://www.gnu.org/licenses/>.
***/

#ifndef sinkinput_row_h
#define sinkinput_row_h

#include "pulsecontrol.h"

#include "stream-row.h"

class MainWindow;

class SinkInputRow : public StreamRow {
public:
    SinkInputRow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x, const pa_sink_input_info &info);
    static SinkInputRow* create(ContainerWidget *container, const pa_sink_input_info &info);
    ~SinkInputRow(void);

    void updateSinkInput(const pa_sink_input_info &info, char* client_name, std::list<DeviceComboBoxItem> sinks);
    void updateClient(const pa_client_info &info);
    void updateAvailableSinks(std::list<DeviceComboBoxItem> sinks);

    uint32_t sinkIndex();

private:
    virtual void executeVolumeUpdate();
    virtual void onMuteToggleButton();
    virtual void onKill();
    virtual void onDeviceComboBoxChanged();

    SinkInputType type;
    uint32_t index, clientIndex, mSinkIndex;
};

#endif
