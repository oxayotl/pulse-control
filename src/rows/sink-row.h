/***
  This file is part of pulsecontrol.

  Copyright 2006-2008 Lennart Poettering
  Copyright 2009 Colin Guthrie

  pulsecontrol is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  pulsecontrol is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pulsecontrol. If not, see <http://www.gnu.org/licenses/>.
***/

#ifndef sink_row_h
#define sink_row_h

#include "pulsecontrol.h"
#include "device-row.h"

#if HAVE_EXT_DEVICE_RESTORE_API
#  include <pulse/format.h>

#  define PAVU_NUM_ENCODINGS 8

typedef struct {
    pa_encoding encoding;
    Gtk::CheckButton *widget;
} encodingList;
#endif

class SinkRow : public DeviceRow {
public:
    SinkRow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x, const pa_sink_info &info, bool canRenameWidget);
    ~SinkRow();
    static SinkRow* create(ContainerWidget *container, bool canRenameWidget, const pa_sink_info &info);

    void updateSink(const pa_sink_info &info);
#if HAVE_EXT_DEVICE_RESTORE_API
    void updateDeviceInfo(const pa_ext_device_restore_info &info);
#endif

    uint32_t monitor_index;

    void setDigital(bool);

protected:
    bool hasActiveSink(const pa_sink_info &info);
#if HAVE_EXT_DEVICE_RESTORE_API
    encodingList encodings[PAVU_NUM_ENCODINGS];
    Gtk::Grid *encodingSelect;
#endif

    SinkType type;
    bool can_decibel;
    uint32_t owner_module_index;

    virtual void executeVolumeUpdate();
    virtual void onMuteToggleButton();
    virtual void onDefaultToggleButton();
    virtual void onKill();

    virtual void onPortChange();
    virtual void onEncodingsChange();

    void initializeEncodingWidgets(const Glib::RefPtr<Gtk::Builder>& x);

    ca_context *canberraContext;
};

#endif
