/***
  This file is part of pulsecontrol.

  Copyright 2006-2008 Lennart Poettering
  Copyright 2009 Colin Guthrie

  pulsecontrol is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  pulsecontrol is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pulsecontrol. If not, see <http://www.gnu.org/licenses/>.
***/

#ifndef sourceoutput_row_h
#define sourceoutput_row_h

#include "pulsecontrol.h"

#include "stream-row.h"

class MainWindow;

class SourceOutputRow : public StreamRow {
public:
    SourceOutputRow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x, const pa_source_output_info &info);
    static SourceOutputRow* create(ContainerWidget *container, const pa_source_output_info &info);
    ~SourceOutputRow(void);

    void updateSourceOutput(const pa_source_output_info &info, char* client_name, std::list<DeviceComboBoxItem> sources);
    void updateClient(const pa_client_info &info);
    void updateAvailableSources(std::list<DeviceComboBoxItem> sources);

    uint32_t sourceIndex();

private:
#if HAVE_SOURCE_OUTPUT_VOLUMES
    virtual void executeVolumeUpdate();
    virtual void onMuteToggleButton();
#endif
    virtual void onKill();
    virtual void onDeviceComboBoxChanged();

    SourceOutputType type;
    uint32_t index, clientIndex, mSourceIndex;
};

#endif

