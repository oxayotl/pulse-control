/***
  This file is part of pulsecontrol.

  Copyright 2006-2008 Lennart Poettering
  Copyright 2009 Colin Guthrie

  pulsecontrol is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  pulsecontrol is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pulsecontrol. If not, see <http://www.gnu.org/licenses/>.
***/

#include "sourceoutput-row.h"
#include "mainwindow.h"
#include "source-row.h"
#include "widgets/pulsecontroldropdown.h"

#include "i18n.h"

SourceOutputRow::SourceOutputRow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x, const pa_source_output_info &info) :
    StreamRow(cobject, x) {

    gchar *txt;
    directionLabel->set_label(txt = g_markup_printf_escaped("<i>%s</i>", _("from")));
    g_free(txt);

    addKillButton(_("Terminate Recording"));

    index = info.index;

#if HAVE_SOURCE_OUTPUT_VOLUMES
    setChannelMap(info.channel_map, true);
#elif
    /* Source Outputs do not have volume controls in versions of PA < 1.0 */
    muteToggleButton->hide();
    lockToggleButton->hide();
#endif
}

SourceOutputRow* SourceOutputRow::create(ContainerWidget *container, const pa_source_output_info &info) {
    SourceOutputRow* w;
    Glib::RefPtr<Gtk::Builder> x = Gtk::Builder::create_from_resource("/pulsecontrol/ui/rows/stream-row.ui");
    w = Gtk::Builder::get_widget_derived<SourceOutputRow>(x, "streamWidget", info);
    container->addWidget(w);
    w->container = container;
    return w;
}

SourceOutputRow::~SourceOutputRow(void) {
}

void SourceOutputRow::updateSourceOutput(const pa_source_output_info &info, char* client_name, std::list<DeviceComboBoxItem> sources){
    clientIndex = info.client;
    type = info.client != PA_INVALID_INDEX ? SOURCE_OUTPUT_CLIENT : SOURCE_OUTPUT_VIRTUAL;

    char *txt;
    if (client_name != NULL) {
        boldNameLabel->set_markup(txt = g_markup_printf_escaped("<b>%s</b>", client_name));
        g_free(txt);
        nameLabel->set_markup(txt = g_markup_printf_escaped(": %s", info.name));
        g_free(txt);
    } else {
        boldNameLabel->set_text("");
        nameLabel->set_label(info.name);
    }
    nameLabel->set_tooltip_text(info.name);

    mSourceIndex = info.source;
    updateAvailableSources(sources);

    setIcon(info.proplist);

#if HAVE_SOURCE_OUTPUT_VOLUMES
    setVolume(info.volume);

    disable_callback = true;
    muteToggleButton->set_active(info.mute);
    disable_callback = false;
#endif

}

void SourceOutputRow::updateClient(const pa_client_info &info) {
    if (clientIndex == info.index) {
        gchar *txt;
        boldNameLabel->set_markup(txt = g_markup_printf_escaped("<b>%s</b>", info.name));
        g_free(txt);
    }
}

uint32_t SourceOutputRow::sourceIndex() {
    return mSourceIndex;
}

void SourceOutputRow::updateAvailableSources(std::list<DeviceComboBoxItem> sources) {
    Glib::ustring currentSourceName = UNKNOWN_DEVICE_NAME;

    disable_callback = true;
    deviceComboBox->remove_all();

    for (auto dev = sources.begin(); dev != sources.end(); dev++) {
        deviceComboBox->append(dev->name, dev->description, dev->priority, dev->availability);
        if (dev->index == mSourceIndex)
            currentSourceName = dev->name;
    }

    if (currentSourceName == UNKNOWN_DEVICE_NAME)
        deviceComboBox->append(UNKNOWN_DEVICE_NAME, _("Unknown input"), 0, false);

    deviceComboBox->set_active_id(currentSourceName);
    disable_callback = false;

    bool multipleSources = sources.size() > 1;

    if (multipleSources) {
        directionLabel->show();
        deviceComboBox->show();
    } else {
        directionLabel->hide();
        deviceComboBox->hide();
    }
}

#if HAVE_SOURCE_OUTPUT_VOLUMES
void SourceOutputRow::executeVolumeUpdate() {
    pa_operation* o;

    if (!(o = pa_context_set_source_output_volume(get_context(), index, &volume, NULL, NULL))) {
        show_error(this, _("pa_context_set_source_output_volume() failed"));
        return;
    }

    pa_operation_unref(o);
}

void SourceOutputRow::onMuteToggleButton() {
    StreamRow::onMuteToggleButton();

    if (disable_callback)
        return;

    pa_operation* o;
    if (!(o = pa_context_set_source_output_mute(get_context(), index, muteToggleButton->get_active(), NULL, NULL))) {
        show_error(this, _("pa_context_set_source_output_mute() failed"));
        return;
    }

    pa_operation_unref(o);
}
#endif

void SourceOutputRow::onKill() {
    pa_operation* o;
    if (!(o = pa_context_kill_source_output(get_context(), index, NULL, NULL))) {
        show_error(this, _("pa_context_kill_source_output() failed"));
        return;
    }

    pa_operation_unref(o);
}

void SourceOutputRow::onDeviceComboBoxChanged() {
    if (disable_callback)
        return;

    Glib::ustring sourceName = deviceComboBox->get_active_id();

    pa_operation *o = pa_context_move_source_output_by_name(get_context(), index, sourceName.c_str(), NULL, NULL);
    if (o)
        pa_operation_unref(o);
}

