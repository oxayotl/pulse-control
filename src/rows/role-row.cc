/***
  This file is part of pulsecontrol.

  Copyright 2006-2008 Lennart Poettering
  Copyright 2009 Colin Guthrie

  pulsecontrol is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  pulsecontrol is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pulsecontrol. If not, see <http://www.gnu.org/licenses/>.
***/

#include "role-row.h"
#include "widgets/containerwidget.h"
#include "widgets/pulsecontroldropdown.h"

#include <pulse/ext-stream-restore.h>

#include "i18n.h"

RoleRow::RoleRow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x, const std::string mediarole, const char* label) :
    StreamRow(cobject, x) {

    lockToggleButton->hide();
    directionLabel->hide();
    deviceComboBox->hide();
    boldNameLabel->set_text("");
    iconImage->set_from_icon_name("notification-sound-symbolic");

    role = "sink-input-by-media-role:" + mediarole;
    nameLabel->set_label(label);

    pa_channel_map cm = {
        1, { PA_CHANNEL_POSITION_MONO }
    };
    setChannelMap(cm, true);

}

RoleRow* RoleRow::create(ContainerWidget *container, const std::string mediarole, const char* label) {
    RoleRow* w;
    Glib::RefPtr<Gtk::Builder> x = Gtk::Builder::create_from_resource("/pulsecontrol/ui/rows/stream-row.ui");
    w = Gtk::Builder::get_widget_derived<RoleRow>(x, "streamWidget", mediarole, label);

    container->addWidget(w);
    return w;
}

void RoleRow::update(const pa_ext_stream_restore_info &info) {
    disable_callback = true;
    device = info.device ? info.device : "";

    pa_cvolume volume;
    volume.channels = 1;
    volume.values[0] = pa_cvolume_max(&info.volume);

    setVolume(volume);
    muteToggleButton->set_active(info.mute);

    disable_callback = false;
}



void RoleRow::onMuteToggleButton() {
    StreamRow::onMuteToggleButton();

    executeVolumeUpdate();
}

void RoleRow::executeVolumeUpdate() {
    pa_ext_stream_restore_info info;

    if (disable_callback)
        return;

    info.name = role.c_str();
    info.channel_map.channels = 1;
    info.channel_map.map[0] = PA_CHANNEL_POSITION_MONO;
    info.volume = volume;
    info.device = device == "" ? NULL : device.c_str();
    info.mute = muteToggleButton->get_active();

    pa_operation* o;
    if (!(o = pa_ext_stream_restore_write(get_context(), PA_UPDATE_REPLACE, &info, 1, TRUE, NULL, NULL))) {
        show_error(this, _("pa_ext_stream_restore_write() failed"));
        return;
    }

    pa_operation_unref(o);
}

