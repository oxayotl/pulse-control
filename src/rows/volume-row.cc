/***
  This file is part of pulsecontrol.

  Copyright 2006-2008 Lennart Poettering
  Copyright 2009 Colin Guthrie

  pulsecontrol is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  pulsecontrol is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pulsecontrol. If not, see <http://www.gnu.org/licenses/>.
***/

#include "volume-row.h"
#include "widgets/channelwidget.h"

/*** VolumeRow ***/
VolumeRow::VolumeRow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x) :
    Gtk::ListBoxRow(cobject),
    lastPeak(0),
    volumeMeterEnabled(false),
    disable_callback(false) {
    channelsBox = x->get_widget<Gtk::Box>("channelsBox");
    peakLevelBar = x->get_widget<Gtk::LevelBar>("peakLevelBar");

    iconImage = x->get_widget<Gtk::Image>("iconImage");
    nameLabel = x->get_widget<Gtk::Label>("nameLabel");
    boldNameLabel = x->get_widget<Gtk::Label>("boldNameLabel");

    lockToggleButton = x->get_widget<Gtk::ToggleButton>("lockToggleButton");
    muteToggleButton = x->get_widget<Gtk::ToggleButton>("muteToggleButton");

    killStreamButton = x->get_widget<Gtk::Button>("killStreamButton");
    /* XXX: Why is the peak meter hidden by default? Maybe the idea is that if
     * setting up the monitoring stream fails for whatever reason, then we
     * shouldn't show the peak meter at all. */
    peakLevelBar->hide();
    peakLevelBar->add_offset_value (GTK_LEVEL_BAR_OFFSET_LOW, 0.8);
    peakLevelBar->add_offset_value (GTK_LEVEL_BAR_OFFSET_HIGH, 0.9);
    peakLevelBar->add_offset_value (GTK_LEVEL_BAR_OFFSET_FULL, 1);
}

VolumeRow::~VolumeRow() {
}


void VolumeRow::setChannelMap(const pa_channel_map &m, bool can_decibel) {
    channelMap = m;
    ChannelWidget::create(this, m, can_decibel, channelWidgets);

    for (int i = 0; i < m.channels; i++) {
        ChannelWidget *cw = channelWidgets[i];
        channelsBox->prepend(*cw);
        cw->unreference();
    }

    lockToggleButton->set_sensitive(m.channels > 1);
    hideLockedChannels(lockToggleButton->get_active());
}

void VolumeRow::setVolume(const pa_cvolume &v, bool force) {
    g_assert(v.channels == channelMap.channels);

    volume = v;
    for (int i = 0; i < volume.channels; i++)
        if (volume.values[0] != volume.values[i]) {
            lockToggleButton->set_active(false);
            hideLockedChannels(false);
        }

    if (timeoutConnection.empty() || force) { /* do not update the volume when a volume change is still in flux */
        for (int i = 0; i < volume.channels; i++)
            channelWidgets[i]->setVolume(volume.values[i]);
    }
}

void VolumeRow::updateChannelVolume(int channel, pa_volume_t v) {
    pa_cvolume n;
    g_assert(channel < volume.channels);

    n = volume;
    if (lockToggleButton->get_active()) {
        for (int i = 0; i < n.channels; i++)
            n.values[i] = v;
    } else
        n.values[channel] = v;

    setVolume(n, true);

    if (timeoutConnection.empty())
        timeoutConnection = Glib::signal_timeout().connect(sigc::mem_fun(*this, &VolumeRow::timeoutEvent), 100);
}

#define DECAY_STEP (1.0 / PEAKS_RATE)

void VolumeRow::updatePeak(double v) {
    if (lastPeak >= DECAY_STEP)
        if (v < lastPeak - DECAY_STEP)
            v = lastPeak - DECAY_STEP;

    lastPeak = v;

    if (v >= 0) {
        peakLevelBar->set_sensitive(TRUE);
        peakLevelBar->set_value(v);
    } else {
        peakLevelBar->set_sensitive(FALSE);
        peakLevelBar->set_value(0);
    }

    enableVolumeMeter();
}

void VolumeRow::addKillButton(const char* killLabel) {
    killStreamButton->set_tooltip_text(killLabel);
    killStreamButton->signal_clicked().connect(sigc::mem_fun(*this, &VolumeRow::onKill));
    killStreamButton->set_visible(true);
}

void VolumeRow::enableVolumeMeter() {
    if (volumeMeterEnabled)
        return;

    volumeMeterEnabled = true;
    peakLevelBar->show();
}

bool VolumeRow::timeoutEvent() {
    executeVolumeUpdate();
    return false;
}

void VolumeRow::hideLockedChannels(bool hide) {
    for (int i = 0; i < channelMap.channels - 1; i++)
        channelWidgets[i]->set_visible(!hide);

    channelWidgets[channelMap.channels - 1]->channelLabel->set_visible(!hide);
}

void VolumeRow::onMuteToggleButton() {

    lockToggleButton->set_sensitive(!muteToggleButton->get_active());

    for (int i = 0; i < channelMap.channels; i++)
        channelWidgets[i]->set_sensitive(!muteToggleButton->get_active());
}

void VolumeRow::onLockToggleButton() {
    hideLockedChannels(lockToggleButton->get_active());
}

void VolumeRow::onKill() {
}
