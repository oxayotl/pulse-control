/***
  This file is part of pulsecontrol.

  Copyright 2006-2008 Lennart Poettering
  Copyright 2009 Colin Guthrie

  pulsecontrol is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  pulsecontrol is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pulsecontrol. If not, see <http://www.gnu.org/licenses/>.
***/

#ifndef stream_row_h
#define stream_row_h

#include "pulsecontrol.h"

#include "volume-row.h"

class MainWindow;
class ChannelWidget;
class PulseControlDropDown;

/* Used as the ID for the unknown device item in deviceComboBox. */
#define UNKNOWN_DEVICE_NAME "#unknown#"

class StreamRow : public VolumeRow {
public:
    StreamRow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x);

    void setChannelMap(const pa_channel_map &m, bool can_decibel);

protected:
    Gtk::Label *directionLabel;
    PulseControlDropDown *deviceComboBox;


    virtual void onDeviceComboBoxChanged();

    void setIcon(const pa_proplist *props);
};

#endif

