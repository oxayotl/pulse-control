/***
  This file is part of pulsecontrol.

  Copyright 2006-2008 Lennart Poettering
  Copyright 2009 Colin Guthrie

  pulsecontrol is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  pulsecontrol is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pulsecontrol. If not, see <http://www.gnu.org/licenses/>.
***/

#ifndef role_row_h
#define role_row_h

#include "pulsecontrol.h"

#include "stream-row.h"

class RoleRow : public StreamRow {
public:
    RoleRow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x, const std::string mediarole, const char* label);
    static RoleRow* create(ContainerWidget *container, const std::string mediarole, const char* label);
    void update(const pa_ext_stream_restore_info &info);

protected:
    Glib::ustring role;
    Glib::ustring device;

    virtual void onMuteToggleButton();
    virtual void executeVolumeUpdate();
};

#endif
