/***
  This file is part of pulsecontrol.

  Copyright 2006-2008 Lennart Poettering
  Copyright 2009 Colin Guthrie

  pulsecontrol is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  pulsecontrol is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pulsecontrol. If not, see <http://www.gnu.org/licenses/>.
***/

#include "card-row.h"
#include "widgets/containerwidget.h"
#include "widgets/pulsecontroldropdown.h"

#include "i18n.h"

/*** CardRow ***/
CardRow::CardRow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x) :
    Gtk::ListBoxRow(cobject),
    icon_name(NULL),
    icon_theme(Gtk::IconTheme::get_for_display(Gdk::Display::get_default())) {

    nameLabel = x->get_widget<Gtk::Label>("nameLabel");
    profileList = Gtk::Builder::get_widget_derived<PulseControlDropDown>(x, "profileList");
    iconImage = x->get_widget<Gtk::Image>("iconImage");
    codecBox = x->get_widget<Gtk::Box>("codecBox");
    codecList =  Gtk::Builder::get_widget_derived<PulseControlDropDown>(x, "codecList");
    profileLockToggleButton = x->get_widget<Gtk::ToggleButton>("profileLockToggleButton");

    profileList->connect_property_changed("selected",  sigc::mem_fun(*this, &CardRow::onProfileChange));

    codecBox->hide();


    codecList->connect_property_changed("selected",  sigc::mem_fun(*this, &CardRow::onCodecChange));

    profileLockToggleButton->signal_clicked().connect(sigc::mem_fun(*this, &CardRow::onProfileLockToggleButton));
    profileLockToggleButton->set_sensitive(true);
    profileLockToggleButton->set_visible(false);

    icon_theme->signal_changed().connect(sigc::mem_fun(*this, &CardRow::setIcon));
}

CardRow* CardRow::create(ContainerWidget *container, const pa_card_info &info) {
    CardRow* w;
    Glib::RefPtr<Gtk::Builder> x = Gtk::Builder::create_from_resource("/pulsecontrol/ui/rows/card-row.ui");
    w = Gtk::Builder::get_widget_derived<CardRow>(x, "cardWidget");
    container->addWidget(w);
    w->index = info.index;
    w->update(info);
    return w;
}

bool is_profile_present_on_port(const pa_card_profile_info2* profile, const pa_card_port_info* port) {
    if (port->profiles2 == NULL)
        return false;
    for (int i = 0; ; i++) {
        if (port->profiles2[i] == NULL)
            return false;
        if (strcmp(port->profiles2[i]->name, profile->name) == 0)
            return true;
    }
}


void CardRow::update(const pa_card_info &info) {

    pulse_card_name = info.name;

    const char *description = pa_proplist_gets(info.proplist, PA_PROP_DEVICE_DESCRIPTION);
    nameLabel->set_text(description ? description : info.name);

    if (icon_name == NULL || strcmp (pa_proplist_gets(info.proplist, PA_PROP_DEVICE_ICON_NAME), icon_name) != 0) {
        icon_name = strdup(pa_proplist_gets(info.proplist, PA_PROP_DEVICE_ICON_NAME));
        setIcon();
    }

    disable_callback = true;

    profileList->remove_all();
    Glib::ustring activeProfile = info.active_profile ? info.active_profile->name : "";
    for (uint32_t i = 0; i < info.n_ports; i++) {
        pa_card_port_info* port = info.ports[i];
        std::string port_name = port->name;
        latency_offset_by_port_name[port_name] = port->latency_offset;
    }


    for (uint32_t i=0; i<info.n_profiles; ++i) {
        bool hasNo = false, hasOther = false;

        pa_card_profile_info2 *current_iteration_profile = info.profiles2[i];
        Glib::ustring desc = current_iteration_profile->description;
        gboolean available = true;

        for (uint32_t j = 0; j < info.n_ports; j++) {
            pa_card_port_info* port = info.ports[j];
            if (is_profile_present_on_port(current_iteration_profile, port)) {
                if (port->available == PA_PORT_AVAILABLE_NO)
                    hasNo = true;
                else {
                    hasOther = true;
                    break;
                }
            }
        }

        if (hasNo && !hasOther) {
            desc += _(" (unplugged)");
            available = false;
        }

        if (!current_iteration_profile->available) {
            desc += _(" (unavailable)");
            available = false;
        }

        profileList->append(current_iteration_profile->name, desc, current_iteration_profile->priority, available);
    }

    profileList->set_active_id(activeProfile);

    disable_callback = false;
}

void CardRow::updateCardCodecs(const std::string& card_name, const std::unordered_map<std::string, std::string>& newCodecs) {
    if (card_name != pulse_card_name) {
        return;
    }

    disable_callback = true;
    codecList->remove_all();

    /* insert profiles */
    for (auto e : newCodecs) {
        codecList->append(e.first, e.second, 0, true);
    }

    codecList->set_active_id(activeCodec);

    /* unhide codec box */
    if (newCodecs.size())
        codecBox->show();
    else
        codecBox->hide();
    disable_callback = false;
}

void CardRow::setActiveCodec(const std::string& card_name, const std::string& codec) {
    if (card_name != pulse_card_name) {
        return;
    }

    activeCodec = codec;
    codecList->set_active_id(activeCodec);
}


void CardRow::setCardProfileIsSticky(const std::string& card_name, gboolean profile_is_sticky) {
    if (card_name != pulse_card_name) {
        return;
    }

    disable_callback = true;

    /* make sure that profile lock toggle button is visible */
    profileLockToggleButton->set_visible(true);
    profileLockToggleButton->set_active(profile_is_sticky);

    disable_callback = false;
}

std::map<std::string,int64_t> CardRow::get_latency_offset_for_all_ports() {
    return latency_offset_by_port_name;
}

void CardRow::setIcon() {
    if (icon_name == NULL)
        return;
    char* icon = strdup(icon_name);
    if (icon) {
        std::string iconName(icon);
        while (!icon_theme->has_icon((iconName + "-symbolic").c_str())) {
            size_t lastDashIndex = iconName.find_last_of("-");
            if (lastDashIndex == std::string::npos) {
                g_free(icon);
                icon = strdup("audio-card-symbolic");
                break;
            }
            iconName = iconName.substr(0,lastDashIndex);
            g_free(icon);
            icon = strdup((iconName + "-symbolic").c_str());
        }
    }
    iconImage->set_from_icon_name(icon);
    g_free(icon);

}

void CardRow::onProfileChange() {
    if (disable_callback)
        return;

    Glib::ustring profile = profileList->get_active_id();

    pa_operation* o;
    if (!(o = pa_context_set_card_profile_by_index(get_context(), index, profile.c_str(), NULL, NULL))) {
      show_error(this, _("pa_context_set_card_profile_by_index() failed"));
      return;
    }

    pa_operation_unref(o);
}

void CardRow::onCodecChange() {
    if (disable_callback)
        return;
#ifdef HAVE_PULSE_MESSAGING_API
    Glib::ustring codec_id = codecList->get_active_id();

    pa_operation* o;
    std::string codec_message = "\"" + std::string(codec_id) + "\"";
    if (!(o = pa_context_send_message_to_object(get_context(), card_bluez_message_handler_path(pulse_card_name).c_str(),
        "switch-codec", codec_message.c_str(), NULL, NULL))) {
        g_debug(_("pa_context_send_message_to_object() failed: %s"), pa_strerror(pa_context_errno(get_context())));
        return;
    }
    pa_operation_unref(o);
#endif
}

void CardRow::onProfileLockToggleButton() {
    if (disable_callback)
        return;

#ifdef HAVE_PULSE_MESSAGING_API
    Glib::ustring profile = profileList->get_active_id();

    bool profileIsLocked = profileLockToggleButton->get_active();

    pa_operation* o;
    if (!(o = pa_context_send_message_to_object(get_context(), card_message_handler_path(pulse_card_name).c_str(),
        "set-profile-sticky", profileIsLocked ? "true" : "false", NULL, NULL))) {
        g_debug(_("pa_context_send_message_to_object() failed: %s"), pa_strerror(pa_context_errno(get_context())));
        return;
    }
    pa_operation_unref(o);
#endif
}

