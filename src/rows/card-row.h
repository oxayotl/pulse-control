/***
  This file is part of pulsecontrol.

  Copyright 2006-2008 Lennart Poettering
  Copyright 2009 Colin Guthrie

  pulsecontrol is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  pulsecontrol is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pulsecontrol. If not, see <http://www.gnu.org/licenses/>.
***/

#ifndef card_row_h
#define card_row_h

#include "pulsecontrol.h"

class PulseControlDropDown;

class CardRow : public Gtk::ListBoxRow {
public:
    CardRow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x);
    static CardRow* create(ContainerWidget *container, const pa_card_info &info);
    void update(const pa_card_info &info);
    void updateCardCodecs(const std::string& card_name, const std::unordered_map<std::string, std::string>& codecs);
    void setActiveCodec(const std::string& card_name, const std::string& codec);
    void setCardProfileIsSticky(const std::string& card_name, gboolean profile_is_sticky);
    std::map<std::string,int64_t> get_latency_offset_for_all_ports();

protected:
    Gtk::Label *nameLabel;
    Gtk::Image *iconImage;
    Gtk::Box *codecBox;
    Gtk::ToggleButton *profileLockToggleButton;

    PulseControlDropDown *profileList;

    PulseControlDropDown *codecList;
    Glib::RefPtr<Gtk::ListStore> codecListStore;

    uint32_t index;
    std::string pulse_card_name;
    // Disactivate callbacks since changes in buttons states, comboxbox selected items etc
    // aren't new user commands but status change reflection
    bool disable_callback;
    // each entry in profiles is a pair of profile name and profile description
    std::vector<std::pair<Glib::ustring, Glib::ustring>> profiles;
    std::map<std::string, int64_t> latency_offset_by_port_name;
    bool hasSinks;
    bool hasSources;
    Glib::ustring activeCodec;

    bool hasProfileLock;

    const char* icon_name;
    Glib::RefPtr<Gtk::IconTheme> icon_theme;
    void setIcon();

    virtual void onProfileChange();
    virtual void onCodecChange();
    virtual void onProfileLockToggleButton();
};

#endif

