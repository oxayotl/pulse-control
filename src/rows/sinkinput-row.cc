/***
  This file is part of pulsecontrol.

  Copyright 2006-2008 Lennart Poettering
  Copyright 2009 Colin Guthrie

  pulsecontrol is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  pulsecontrol is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pulsecontrol. If not, see <http://www.gnu.org/licenses/>.
***/

#include "sinkinput-row.h"
#include "widgets/containerwidget.h"
#include "widgets/pulsecontroldropdown.h"

#include "i18n.h"

SinkInputRow::SinkInputRow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x, const pa_sink_input_info &info) :
    StreamRow(cobject, x) {

    gchar *txt;
    directionLabel->set_label(txt = g_markup_printf_escaped("<i>%s</i>", _("on")));
    g_free(txt);

    index = info.index;
    setChannelMap(info.channel_map, true);

    addKillButton(_("Terminate Playback"));
}

SinkInputRow* SinkInputRow::create(ContainerWidget *container, const pa_sink_input_info &info) {
    SinkInputRow* w;
    Glib::RefPtr<Gtk::Builder> x = Gtk::Builder::create_from_resource("/pulsecontrol/ui/rows/stream-row.ui");
    w = Gtk::Builder::get_widget_derived<SinkInputRow>(x, "streamWidget", info);

    container->addWidget(w);
    w->container = container;

    return w;
}

SinkInputRow::~SinkInputRow(void) {
}

void SinkInputRow::updateSinkInput(const pa_sink_input_info &info, char* client_name, std::list<DeviceComboBoxItem> sinks) {
    clientIndex = info.client;
    type = info.client != PA_INVALID_INDEX ? SINK_INPUT_CLIENT : SINK_INPUT_VIRTUAL;

    if (client_name) {
        char *txt;
        boldNameLabel->set_markup(txt = g_markup_printf_escaped("<b>%s</b>", client_name));
        g_free(txt);
        nameLabel->set_markup(txt = g_markup_printf_escaped(": %s", info.name));
        g_free(txt);
    } else {
        boldNameLabel->set_text("");
        nameLabel->set_label(info.name);
    }

    nameLabel->set_tooltip_text(info.name);

    mSinkIndex = info.sink;
    updateAvailableSinks(sinks);

    setIcon(info.proplist);

    setVolume(info.volume);

    disable_callback = true;
    muteToggleButton->set_active(info.mute);
    disable_callback = false;

}

void SinkInputRow::updateClient(const pa_client_info &info) {
    if (clientIndex == info.index) {
        gchar *txt;
        boldNameLabel->set_markup(txt = g_markup_printf_escaped("<b>%s</b>", info.name));
        g_free(txt);
    }
}

void SinkInputRow::updateAvailableSinks(std::list<DeviceComboBoxItem> sinks) {
    Glib::ustring currentSinkName = UNKNOWN_DEVICE_NAME;
    disable_callback = true;

    deviceComboBox->remove_all();

    for (auto dev = sinks.begin(); dev != sinks.end(); dev++) {

        deviceComboBox->append(dev->name, dev->description, dev->priority, dev->availability);

        if (dev->index == mSinkIndex)
            currentSinkName = dev->name;
    }

    if (currentSinkName == UNKNOWN_DEVICE_NAME)
        deviceComboBox->append(UNKNOWN_DEVICE_NAME, _("Unknown output"), 0, false);

    deviceComboBox->set_active_id(currentSinkName);
    disable_callback = false;

    bool multipleSinks = sinks.size() > 1;

    if (multipleSinks) {
        directionLabel->show();
        deviceComboBox->show();
    } else {
        directionLabel->hide();
        deviceComboBox->hide();
    }
}

uint32_t SinkInputRow::sinkIndex() {
    return mSinkIndex;
}

void SinkInputRow::executeVolumeUpdate() {
    pa_operation* o;

    if (!(o = pa_context_set_sink_input_volume(get_context(), index, &volume, NULL, NULL))) {
        show_error(this, _("pa_context_set_sink_input_volume() failed"));
        return;
    }

    pa_operation_unref(o);
}

void SinkInputRow::onMuteToggleButton() {
    StreamRow::onMuteToggleButton();

    if (disable_callback)
        return;

    pa_operation* o;
    if (!(o = pa_context_set_sink_input_mute(get_context(), index, muteToggleButton->get_active(), NULL, NULL))) {
        show_error(this, _("pa_context_set_sink_input_mute() failed"));
        return;
    }

    pa_operation_unref(o);
}

void SinkInputRow::onKill() {
    pa_operation* o;
    if (!(o = pa_context_kill_sink_input(get_context(), index, NULL, NULL))) {
        show_error(this, _("pa_context_kill_sink_input() failed"));
        return;
    }

    pa_operation_unref(o);
}

void SinkInputRow::onDeviceComboBoxChanged() {
    if (disable_callback)
        return;

    Glib::ustring sinkName = deviceComboBox->get_active_id();
    pa_operation *o = pa_context_move_sink_input_by_name(get_context(), index, sinkName.c_str(), NULL, NULL);
    if (o)
        pa_operation_unref(o);
}
