/***
  This file is part of pulsecontrol.

  Copyright 2006-2008 Lennart Poettering
  Copyright 2009 Colin Guthrie

  pulsecontrol is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  pulsecontrol is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pulsecontrol. If not, see <http://www.gnu.org/licenses/>.
***/

#include "source-row.h"
#include "widgets/pulsecontroldropdown.h"

#include "i18n.h"

SourceRow::SourceRow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x, const pa_source_info &info, bool canRenameWidget) :
    DeviceRow(cobject, x, "source", canRenameWidget, "audio-input-microphone") {
    index = info.index;
    setChannelMap(info.channel_map, !!(info.flags & PA_SOURCE_DECIBEL_VOLUME));
    setBaseVolume(info.base_volume);

}

bool SourceRow::hasActiveSource(const pa_source_info &info) {
    for (uint32_t i=0; i<info.n_ports; ++i) {
        if( info.ports[i]->available == PA_PORT_AVAILABLE_YES
        || info.ports[i]->available == PA_PORT_AVAILABLE_UNKNOWN) {
            return true;
        }
    }
    hasActivePort = info.n_ports == 0;
    return hasActivePort;
}

SourceRow* SourceRow::create(ContainerWidget *container, bool canRenameWidget, const pa_source_info &info) {
    SourceRow* w;
    Glib::RefPtr<Gtk::Builder> x = Gtk::Builder::create_from_resource("/pulsecontrol/ui/rows/device-row.ui");
    w = Gtk::Builder::get_widget_derived<SourceRow>(x, "deviceWidget", info, canRenameWidget);

    if (w->hasActiveSource(info)) {
        container->addWidget(w);
    }
    else {
        container->addHiddenWidget(w);
    }
    w->container = container;
    return w;
}

void SourceRow::updateSource(const pa_source_info &info) {
    card_index = info.card;
    name = info.name;
    description = info.description;
    type = info.monitor_of_sink != PA_INVALID_INDEX ? SOURCE_MONITOR : (info.flags & PA_SOURCE_HARDWARE ? SOURCE_HARDWARE : SOURCE_VIRTUAL);

    boldNameLabel->set_text("");
    gchar *txt;
    nameLabel->set_markup(txt = g_markup_printf_escaped("%s", info.description));
    nameLabel->set_tooltip_text(info.description);
    g_free(txt);

    if (icon_name == NULL || strcmp (pa_proplist_gets(info.proplist, PA_PROP_DEVICE_ICON_NAME), icon_name) != 0) {
        if (pa_proplist_gets(info.proplist, PA_PROP_DEVICE_ICON_NAME) != NULL)
            icon_name = strdup(pa_proplist_gets(info.proplist, PA_PROP_DEVICE_ICON_NAME));
        setIcon();
    }

    setVolume(info.volume);
    disable_callback = true;
    muteToggleButton->set_active(info.mute);
    disable_callback = false;

    activePort = info.active_port ? info.active_port->name : "";

    disable_callback = true;
    portList->remove_all();
    if (info.ports != NULL)
    {
        for (uint32_t i = 0; i < info.n_ports; i++) {
            pa_source_port_info *p = info.ports[i];
            Glib::ustring name = p->name;
            Glib::ustring desc = p->description;

            if (p->available == PA_PORT_AVAILABLE_YES)
                desc +=  _(" (plugged in)");
            else if (p->available == PA_PORT_AVAILABLE_NO) {
                if (name == "analog-output-speaker" ||
                    name == "analog-input-microphone-internal")
                    desc += _(" (unavailable)");
                else
                    desc += _(" (unplugged)");
            }
            bool available = (p->available == PA_PORT_AVAILABLE_YES) || (p->available == PA_PORT_AVAILABLE_UNKNOWN);
            portList->append(name, desc, p->priority, available);
        }
        portList->set_active_id(activePort);
    }
    disable_callback = false;

    if (info.n_ports > 0) {
        portSelect->show();

        if (pa_context_get_server_protocol_version(get_context()) >= 27)
            offsetSelect->show();
        else
            offsetSelect->hide();

    } else {
        portSelect->hide();
        offsetSelect->hide();
    }

    updateAdvancedOptionsVisibility();

    updateAdvancedOptionsVisibility();

    container->updateHiddenStatus(this, !hasActiveSource(info));
}


void SourceRow::executeVolumeUpdate() {
    pa_operation* o;

    if (!(o = pa_context_set_source_volume_by_index(get_context(), index, &volume, NULL, NULL))) {
        show_error(this, _("pa_context_set_source_volume_by_index() failed"));
        return;
    }

    pa_operation_unref(o);
}

void SourceRow::onMuteToggleButton() {
    DeviceRow::onMuteToggleButton();

    if (disable_callback)
        return;

    pa_operation* o;
    if (!(o = pa_context_set_source_mute_by_index(get_context(), index, muteToggleButton->get_active(), NULL, NULL))) {
        show_error(this, _("pa_context_set_source_mute_by_index() failed"));
        return;
    }

    pa_operation_unref(o);
}

void SourceRow::onDefaultToggleButton() {
    pa_operation* o;

    if (disable_callback)
        return;

    if (!(o = pa_context_set_default_source(get_context(), name.c_str(), NULL, NULL))) {
        show_error(this, _("pa_context_set_default_source() failed"));
        return;
    }
    pa_operation_unref(o);
}

void SourceRow::onPortChange() {
    if (disable_callback)
        return;

    pa_operation* o;
    Glib::ustring port = portList->get_active_id();

    if (!(o = pa_context_set_source_port_by_index(get_context(), index, port.c_str(), NULL, NULL))) {
    show_error(this, _("pa_context_set_source_port_by_index() failed"));
    return;
    }

    pa_operation_unref(o);
}

