#include "frame-playback.h"
#include "rows/sinkinput-row.h"
#include "rows/role-row.h"
#include "widgets/containerwidget.h"

#include "i18n.h"


FrameSinkInput::FrameSinkInput(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x, gint showSinkInputType) :
Gtk::Widget(cobject),
eventRoleRow(NULL) {
    streamsBox = x->get_widget<Gtk::Box>("streamsBox");
    showRolesButton = x->get_widget<Gtk::ToggleButton>("showRolesButton");
    showApplicationSinkInputStreamsButton = x->get_widget<Gtk::ToggleButton>("showApplicationSinkInputStreamsButton");
    showVirtualSinkInputStreamsButton = x->get_widget<Gtk::ToggleButton>("showVirtualSinkInputStreamsButton");
    noSinkInputTypeSelected = x->get_widget<Gtk::Widget>("noSinkInputTypeSelected");

    roleContainer = ContainerWidget::create(_("Default"), NULL, NULL);
    streamsBox->append(*roleContainer);
    applicationSinkInputsContainer = ContainerWidget::create(_("Applications"), NULL, _("<i>No application is currently playing audio.</i>"));
    streamsBox->append(*applicationSinkInputsContainer);
    virtualSinkInputsContainer = ContainerWidget::create(_("Virtual Streams"), NULL, _("<i>No virtual stream.</i>"));
    streamsBox->append(*virtualSinkInputsContainer);

    if (showSinkInputType != -1) {
        showRolesButton->set_active(showSinkInputType & SINK_INPUT_ROLE);
        showApplicationSinkInputStreamsButton->set_active(showSinkInputType & SINK_INPUT_CLIENT);
        showVirtualSinkInputStreamsButton->set_active(showSinkInputType & SINK_INPUT_VIRTUAL);
    }

    showRolesButton->signal_toggled().connect(sigc::mem_fun(*this, &FrameSinkInput::onVisibleSinkInputTypeChanged));
    showApplicationSinkInputStreamsButton->signal_toggled().connect(sigc::mem_fun(*this, &FrameSinkInput::onVisibleSinkInputTypeChanged));
    showVirtualSinkInputStreamsButton->signal_toggled().connect(sigc::mem_fun(*this, &FrameSinkInput::onVisibleSinkInputTypeChanged));
    onVisibleSinkInputTypeChanged();
}

FrameSinkInput* FrameSinkInput::create(gint showSinkInputType) {
    FrameSinkInput* f;
    Glib::RefPtr<Gtk::Builder> x = Gtk::Builder::create_from_resource("/pulsecontrol/ui/frames/frame-playback.ui");
    f = Gtk::Builder::get_widget_derived<FrameSinkInput>(x, "playbackframe", showSinkInputType);
    f->reference();
    return f;
}


bool FrameSinkInput::updateSinkInput(const pa_sink_input_info &info, char* client_name) {
    SinkInputRow *w;
    bool require_monitor = false;

    const char *t;
    if ((t = pa_proplist_gets(info.proplist, "module-stream-restore.id"))) {
        if (strcmp(t, "sink-input-by-media-role:event") == 0) {
            g_debug(_("Ignoring sink-input due to it being designated as an event and thus handled by the Event widget"));
            return false;
        }
    }

    if (sinkInputWidgets.count(info.index)) {
        w = sinkInputWidgets.at(info.index);
        if (pa_context_get_server_protocol_version(get_context()) >= 13)
            if (w->sinkIndex() != info.sink)
                require_monitor = true;
    } else {
        w = SinkInputRow::create(findContainer(info), info);
        sinkInputWidgets.emplace(info.index, w);

        if (pa_context_get_server_protocol_version(get_context()) >= 13)
            require_monitor = true;
    }

    w->updateSinkInput(info, client_name, sinks);
    return require_monitor;
}

void FrameSinkInput::updateClient(const pa_client_info &info) {
    for (std::map<uint32_t, SinkInputRow*>::iterator i = sinkInputWidgets.begin(); i != sinkInputWidgets.end(); ++i) {
        SinkInputRow *w = i->second;

        if (!w)
            continue;

        w->updateClient(info);
    }
}

void FrameSinkInput::updateVolumeMeter(uint32_t sink_input_idx, double v) {
    SinkInputRow *w;

    if (sinkInputWidgets.count(sink_input_idx)) {
        w = sinkInputWidgets.at(sink_input_idx);
        w->updatePeak(v);
    }
}

void FrameSinkInput::updateAvailableSinks(std::list<DeviceComboBoxItem> newSinks) {
    sinks = newSinks;
    for (std::map<uint32_t, SinkInputRow*>::iterator i = sinkInputWidgets.begin(); i != sinkInputWidgets.end(); ++i) {
        SinkInputRow* w = i->second;
        w->updateAvailableSinks(sinks);
    }
}


void FrameSinkInput::removeSinkInput(uint32_t index) {
    if (!sinkInputWidgets.count(index))
        return;

    SinkInputRow *widget = sinkInputWidgets.at(index);
    widget->container->removeWidget(widget);
    sinkInputWidgets.erase(index);
}

void FrameSinkInput::removeAllSinkInputs() {
    while (!sinkInputWidgets.empty())
        removeSinkInput(sinkInputWidgets.begin()->first);
}


gint FrameSinkInput::getSinkInputTypeShown() {
    int showSinkInputType = SINK_INPUT_NOTHING;
    if (showRolesButton->get_active()) {
        showSinkInputType |= SINK_INPUT_ROLE;
    }
    if (showApplicationSinkInputStreamsButton->get_active()) {
        showSinkInputType |= SINK_INPUT_CLIENT;
    }
    if (showVirtualSinkInputStreamsButton->get_active()) {
        showSinkInputType |= SINK_INPUT_VIRTUAL;
    }

    return showSinkInputType;
}


void FrameSinkInput::updateRole(const pa_ext_stream_restore_info &info) {
    // TODO Add other kind of role widgets ?
    // "video": for movie/video streams from media players
    // "music": for music streams from media players
    // "game": for audio from games
    // "event": for event sounds (i.e. button clicks, ...)
    // "phone": for phone data (i.e. voip speech audio)
    // "animation": for animations (i.e. Flash)
    // "production": for audio production applications.
    // "a11y": accessibility applications (i.e. screen readers, ...)
    //      This is a property of the actual streamed data, not so much the application.
    //      However usually it is still safe to simply set a process-global environment variable.
    if (strcmp(info.name, "sink-input-by-media-role:event") != 0)
        return;

    if (!eventRoleRow) {
        eventRoleRow = RoleRow::create(roleContainer, "event", _("System Sounds"));
    }
    eventRoleRow->update(info);
}

void FrameSinkInput::deleteEventRoleRow() {

    if (eventRoleRow)
        delete eventRoleRow;

    eventRoleRow = NULL;
}


ContainerWidget* FrameSinkInput::findContainer(const pa_sink_input_info &info) {
    return info.client != PA_INVALID_INDEX ? applicationSinkInputsContainer : virtualSinkInputsContainer;
}

void FrameSinkInput::onVisibleSinkInputTypeChanged() {
    roleContainer->set_visible(showRolesButton->get_active());
    applicationSinkInputsContainer->set_visible(showApplicationSinkInputStreamsButton->get_active());
    virtualSinkInputsContainer->set_visible(showVirtualSinkInputStreamsButton->get_active());
    noSinkInputTypeSelected->set_visible(!showRolesButton->get_active()
        && !showApplicationSinkInputStreamsButton->get_active()
        && !showVirtualSinkInputStreamsButton->get_active());
}

