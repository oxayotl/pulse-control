#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "frame-recording.h"
#include "rows/sourceoutput-row.h"
#include "rows/role-row.h"
#include "widgets/containerwidget.h"

#include "i18n.h"


FrameSourceOutput::FrameSourceOutput(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x, gint showSourceOutputType) :
Gtk::Widget(cobject) {
    recsBox = x->get_widget<Gtk::Box>("recsBox");
    noSourceOutputTypeSelected = x->get_widget<Gtk::Widget>("noSourceOutputTypeSelected");
    showApplicationSourceOutputStreamsButton = x->get_widget<Gtk::ToggleButton>("showApplicationSourceOutputStreamsButton");
    showVirtualSourceOutputStreamsButton = x->get_widget<Gtk::ToggleButton>("showVirtualSourceOutputStreamsButton");

    applicationSourceOutputsContainer = ContainerWidget::create(_("Applications"), NULL, _("<i>No application is currently recording audio.</i>"));
    recsBox->append(*applicationSourceOutputsContainer);
    virtualSourceOutputsContainer = ContainerWidget::create(_("Virtual Streams"), NULL, _("<i>No virtual stream is present.</i>"));
    recsBox->append(*virtualSourceOutputsContainer);

    if (showSourceOutputType != -1) {
        showApplicationSourceOutputStreamsButton->set_active(showSourceOutputType & SOURCE_OUTPUT_CLIENT);
        showVirtualSourceOutputStreamsButton->set_active(showSourceOutputType & SOURCE_OUTPUT_VIRTUAL);
    }

    showApplicationSourceOutputStreamsButton->signal_toggled().connect(sigc::mem_fun(*this, &FrameSourceOutput::onVisibleSourceOutputTypeChanged));
    showVirtualSourceOutputStreamsButton->signal_toggled().connect(sigc::mem_fun(*this, &FrameSourceOutput::onVisibleSourceOutputTypeChanged));

    onVisibleSourceOutputTypeChanged();
}

FrameSourceOutput* FrameSourceOutput::create(gint showSourceOutputType){
    FrameSourceOutput* f;
    Glib::RefPtr<Gtk::Builder> x = Gtk::Builder::create_from_resource("/pulsecontrol/ui/frames/frame-recording.ui");
    f = Gtk::Builder::get_widget_derived<FrameSourceOutput>(x, "recordingframe", showSourceOutputType);
    f->reference();
    return f;
}


void FrameSourceOutput::updateSourceOutput(const pa_source_output_info &info, char* client_name){
    SourceOutputRow *w;
    const char *app;

    if ((app = pa_proplist_gets(info.proplist, PA_PROP_APPLICATION_ID)))
        if (strcmp(app, "org.PulseAudio.pavucontrol") == 0
            || strcmp(app, "org.gnome.VolumeControl") == 0
            || strcmp(app, APPLICATION_ID) == 0
            || strcmp(app, "org.kde.kmixd") == 0)
            return;

    if (sourceOutputWidgets.count(info.index))
        w = sourceOutputWidgets.at(info.index);
    else {
        w = SourceOutputRow::create(findContainer(info), info);
        sourceOutputWidgets.emplace(info.index, w);
    }
    w->updateSourceOutput(info, client_name, sources);
}

void FrameSourceOutput::updateClient(const pa_client_info &info){
    for (std::map<uint32_t, SourceOutputRow*>::iterator i = sourceOutputWidgets.begin(); i != sourceOutputWidgets.end(); ++i) {
        SourceOutputRow *w = i->second;

        if (!w)
            continue;
        w->updateClient(info);
    }
}

void FrameSourceOutput::updateVolumeMeter(uint32_t source_index, double v){
    for (std::map<uint32_t, SourceOutputRow*>::iterator i = sourceOutputWidgets.begin(); i != sourceOutputWidgets.end(); ++i) {
        SourceOutputRow* w = i->second;

        if (w->sourceIndex() == source_index)
            w->updatePeak(v);
    }
}

void FrameSourceOutput::removeSourceOutput(uint32_t index){
    if (!sourceOutputWidgets.count(index))
        return;

    SourceOutputRow *w = sourceOutputWidgets.at(index);
    w->container->removeWidget(w);
    sourceOutputWidgets.erase(index);
}

void FrameSourceOutput::removeAllSourceOutput(){
    while (!sourceOutputWidgets.empty())
        removeSourceOutput(sourceOutputWidgets.begin()->first);
}

gint FrameSourceOutput::getSourceOutputTypeShown() {
    int showSourceOutputType = SOURCE_OUTPUT_NOTHING;
    if (showApplicationSourceOutputStreamsButton->get_active()) {
        showSourceOutputType |= SOURCE_OUTPUT_CLIENT;
    }
    if (showVirtualSourceOutputStreamsButton->get_active()) {
        showSourceOutputType |= SOURCE_OUTPUT_VIRTUAL;
    }
    return showSourceOutputType;
}

void FrameSourceOutput::updateAvailableSources(std::list<DeviceComboBoxItem> newSources){
    sources = newSources;

    for (std::map<uint32_t, SourceOutputRow*>::iterator i = sourceOutputWidgets.begin(); i != sourceOutputWidgets.end(); ++i) {
        SourceOutputRow* w = i->second;
        w->updateAvailableSources(sources);
    }
}

void FrameSourceOutput::onVisibleSourceOutputTypeChanged(){
    applicationSourceOutputsContainer->set_visible(showApplicationSourceOutputStreamsButton->get_active());
    virtualSourceOutputsContainer->set_visible(showVirtualSourceOutputStreamsButton->get_active());
    noSourceOutputTypeSelected->set_visible(!showApplicationSourceOutputStreamsButton->get_active() && !showVirtualSourceOutputStreamsButton->get_active());
}

ContainerWidget* FrameSourceOutput::findContainer(const pa_source_output_info &info) {
    return info.client != PA_INVALID_INDEX ? applicationSourceOutputsContainer : virtualSourceOutputsContainer;
}
