#ifndef frame_recording_h
#define frame_recording_h

#include "pulsecontrol.h"

class SourceOutputRow;
class ContainerWidget;

class FrameSourceOutput : public Gtk::Widget {
public:
    FrameSourceOutput(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x, gint showSourceOutputType);
    static FrameSourceOutput* create(gint showSourceOutputType);

    void updateSourceOutput(const pa_source_output_info &info, char* client_name);
    void updateClient(const pa_client_info &info);
    void updateVolumeMeter(uint32_t source_output_index, double v);

    void removeSourceOutput(uint32_t index);
    void removeAllSourceOutput();

    gint getSourceOutputTypeShown();

    void updateAvailableSources(std::list<DeviceComboBoxItem> sources);

protected:
    Gtk::Box *recsBox;
    Gtk::Widget *noSourceOutputTypeSelected;
    ContainerWidget *applicationSourceOutputsContainer, *virtualSourceOutputsContainer;
    Gtk::ToggleButton *showApplicationSourceOutputStreamsButton, *showVirtualSourceOutputStreamsButton;

    std::map<uint32_t, SourceOutputRow*> sourceOutputWidgets;
    std::list<DeviceComboBoxItem> sources;

    ContainerWidget* findContainer(const pa_source_output_info &info);
    virtual void onVisibleSourceOutputTypeChanged();
};
#endif
