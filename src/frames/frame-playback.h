#ifndef frame_playback_h
#define frame_playback_h

#include "pulsecontrol.h"

class SinkInputRow;
class RoleRow;
class ContainerWidget;

class FrameSinkInput : public Gtk::Widget {
public:
    FrameSinkInput(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x, gint showSinkInputType);
    static FrameSinkInput* create(gint showSinkInputType);

    bool updateSinkInput(const pa_sink_input_info &info, char* client_name);
    void updateClient(const pa_client_info &info);
    void updateVolumeMeter(uint32_t sink_input_index, double v);
    void updateAvailableSinks(std::list<DeviceComboBoxItem> sinks);

    void removeSinkInput(uint32_t index);
    void removeAllSinkInputs();

    gint getSinkInputTypeShown();

    void updateRole(const pa_ext_stream_restore_info &info);
    void deleteEventRoleRow();

protected:
    Gtk::Box *streamsBox;
    Gtk::Widget *noSinkInputTypeSelected;
    ContainerWidget *roleContainer, *applicationSinkInputsContainer, *virtualSinkInputsContainer;
    Gtk::ToggleButton *showRolesButton, *showApplicationSinkInputStreamsButton, *showVirtualSinkInputStreamsButton;

    std::map<uint32_t, SinkInputRow*> sinkInputWidgets;
    RoleRow *eventRoleRow;

    std::list<DeviceComboBoxItem> sinks;

    ContainerWidget* findContainer(const pa_sink_input_info &info);
    virtual void onVisibleSinkInputTypeChanged();
};
#endif
