#ifndef frame_config_h
#define frame_config_h

#include "pulsecontrol.h"

class CardRow;
class ContainerWidget;

class FrameConfig : public Gtk::Widget {
public:
    FrameConfig(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x);
    static FrameConfig* create();

    void updateCard(const pa_card_info &info);
    void updateCardCodecs(const std::string& card_name, const std::unordered_map<std::string, std::string>& codecs);
    void setActiveCodec(const std::string& card_name, const std::string& codec);
    void setCardProfileIsSticky(const std::string& card_name, gboolean profile_is_sticky);

    void removeCard(uint32_t index);
    void removeAllCards();

    std::map<std::string,int64_t> findPortLatencyOffsetForCard(uint32_t card_index);

protected:
    Gtk::Box *cardsBox;
    ContainerWidget *cardsContainer;

    std::map<uint32_t, CardRow*> cardWidgets;

};
#endif
