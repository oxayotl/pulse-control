#include "frame-sink.h"
#include "rows/sink-row.h"
#include "widgets/containerwidget.h"

#include "widgets/create-virtual-sink-dialog.h"

#include "pulsecontrolapplication.h"

#include "i18n.h"

FrameSink::FrameSink(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x, gint showSinkType) :
Gtk::Widget(cobject),
canRenameDevices(false),
create_virtual_sink_dialog(NULL) {
    sinksBox = x->get_widget<Gtk::Box>("sinksBox");
    noSinksLabel = x->get_widget<Gtk::Widget>("noSinksLabel");
    hardwareSinksContainer = ContainerWidget::create(_("Hardware Output Devices"), NULL,
    _("<i>No hardware sink dectected.</i>"), _("Unplugged devices"), "unplugged-symbolic");
    sinksBox->append(*hardwareSinksContainer);
    virtualSinksContainer = ContainerWidget::create(_("Virtual Output Devices"), NULL,
    _("<i>No virtual sink dectected.</i>"), _("Unplugged devices"), "unplugged-symbolic");
    sinksBox->append(*virtualSinksContainer);

    Gtk::Button *button = new Gtk::Button();
    button->set_icon_name("list-add-symbolic");
    button->set_has_frame(false);
    button->signal_clicked().connect(sigc::mem_fun(*this, &FrameSink::openCreateVirtualSinkDialog));
    virtualSinksContainer->addSuffix(button);

    showHardwareSinksButton = x->get_widget<Gtk::ToggleButton>("showHardwareSinksButton");
    showVirtualSinksButton = x->get_widget<Gtk::ToggleButton>("showVirtualSinksButton");

    if (showSinkType != -1) {
        showHardwareSinksButton->set_active(showSinkType & SINK_HARDWARE);
        showVirtualSinksButton->set_active(showSinkType & SINK_VIRTUAL);
    }

    showHardwareSinksButton->signal_toggled().connect(sigc::mem_fun(*this, &FrameSink::onVisibleSinkTypeChanged));
    showVirtualSinksButton->signal_toggled().connect(sigc::mem_fun(*this, &FrameSink::onVisibleSinkTypeChanged));
    onVisibleSinkTypeChanged();
}

FrameSink* FrameSink::create(gint showSinkType) {
    FrameSink* f;
    Glib::RefPtr<Gtk::Builder> x = Gtk::Builder::create_from_resource("/pulsecontrol/ui/frames/frame-sink.ui");
    f = Gtk::Builder::get_widget_derived<FrameSink>(x, "sinksframe", showSinkType);
    f->reference();
    return f;
}

ContainerWidget* FrameSink::findContainer(const pa_sink_info &info) {
    return info.flags & PA_SINK_HARDWARE ? hardwareSinksContainer : virtualSinksContainer;
}


bool FrameSink::updateSink(const pa_sink_info &info) {
    SinkRow *w;
    bool is_new = false;

    if (sinkWidgets.count(info.index)) {
        w = sinkWidgets.at(info.index);
    }
    else {
        w = SinkRow::create(findContainer(info), canRenameDevices, info);
        sinkWidgets.emplace(info.index, w);
        is_new = true;
    }

    w->updateSink(info);
    if (info.active_port != NULL) {
        std::string active_port = info.active_port->name;
        if (card_port_latency_offsets[info.card].count(active_port)) {
            w->setLatencyOffset(card_port_latency_offsets[info.card][active_port]);
        }
    }
    w->setIsDefault(defaultSinkName);

    if (is_new && (create_virtual_sink_dialog != NULL)){
        create_virtual_sink_dialog->addSink(w->makeDeviceComboBoxItem());
    }

    return is_new;
}

void FrameSink::updateServer(const pa_server_info &info) {
    defaultSinkName = info.default_sink_name ? info.default_sink_name : "";

    for (std::map<uint32_t, SinkRow*>::iterator i = sinkWidgets.begin(); i != sinkWidgets.end(); ++i) {
        SinkRow *w = i->second;

        if (!w)
            continue;

        w->setIsDefault(defaultSinkName);
    }
}

void FrameSink::updateVolumeMeter(uint32_t source_index, double v){
    for (std::map<uint32_t, SinkRow*>::iterator i = sinkWidgets.begin(); i != sinkWidgets.end(); ++i) {
        SinkRow* w = i->second;

        if (w->monitor_index == source_index)
            w->updatePeak(v);
    }
}

#if HAVE_EXT_DEVICE_RESTORE_API
void FrameSink::updateDeviceInfo(const pa_ext_device_restore_info &info) {
    if (sinkWidgets.count(info.index)) {
        sinkWidgets.at(info.index)->updateDeviceInfo(info);
    }
}
#endif

void FrameSink::updatePortsLatencyOffsetForCard(uint32_t card_index, std::map<std::string,int64_t> port_latency_offsets) {
    card_port_latency_offsets[card_index] = port_latency_offsets;
    for (auto it = sinkWidgets.begin() ; it != sinkWidgets.end(); it++) {
        SinkRow *sw = it->second;
        if (sw->card_index == card_index) {
            if (port_latency_offsets.count(sw->activePort)) {
                sw->setLatencyOffset(port_latency_offsets[sw->activePort]);
            }
        }
    }
}

void FrameSink::removeSink(uint32_t index) {
    if (!sinkWidgets.count(index))
        return;
    SinkRow* w = sinkWidgets.at(index);
    if (create_virtual_sink_dialog != NULL) {
        create_virtual_sink_dialog->removeSink(w->makeDeviceComboBoxItem()->name);
    }
    w->container->removeWidget(w);
    sinkWidgets.erase(index);
}

void FrameSink::removeAllSinks() {
    while (!sinkWidgets.empty())
        removeSink(sinkWidgets.begin()->first);
}


void FrameSink::setCanRenameDevices(bool value) {
    canRenameDevices = value;

    for (std::map<uint32_t, SinkRow*>::iterator i = sinkWidgets.begin(); i != sinkWidgets.end(); ++i) {
        SinkRow *w = i->second;
        w->canRenameDevice(value);
    }
}


uint32_t FrameSink::getMonitorForSink(uint32_t sink_idx) {
    if (!sinkWidgets.count(sink_idx))
        return (uint32_t)-1;
    return sinkWidgets.at(sink_idx)->monitor_index;
}

gint FrameSink::getSinkTypeShown() {
    int showSinkType = SINK_NOTHING;
    if (showHardwareSinksButton->get_active()) {
        showSinkType |= SINK_HARDWARE;
    }
    if (showVirtualSinksButton->get_active()) {
        showSinkType |= SINK_VIRTUAL;
    }
    return showSinkType;
}

std::list<DeviceComboBoxItem> FrameSink::makeDeviceComboBox() {
    std::list<DeviceComboBoxItem> result;
    for (auto i = sinkWidgets.begin(); i != sinkWidgets.end(); i++) {
        SinkRow *sink = i->second;
        if (sink != NULL)
            result.push_front(*sink->makeDeviceComboBoxItem());
    }
    return result;
}


void FrameSink::onVisibleSinkTypeChanged() {
    hardwareSinksContainer->set_visible(showHardwareSinksButton->get_active());
    virtualSinksContainer->set_visible(showVirtualSinksButton->get_active());
    noSinksLabel->set_visible(!showHardwareSinksButton->get_active() && !showVirtualSinksButton->get_active());
}

void FrameSink::openCreateVirtualSinkDialog() {
    create_virtual_sink_dialog = CreateVirtualSinkDialog::create(makeDeviceComboBox());
    create_virtual_sink_dialog->set_transient_for(*PulseControlApplication::get_instance().get_active_window());
    create_virtual_sink_dialog->signal_destroy().connect(sigc::mem_fun(*this, &FrameSink::onCloseCreateVirtualSinkDialog));
    create_virtual_sink_dialog->present();
}


void FrameSink::onCloseCreateVirtualSinkDialog() {
    create_virtual_sink_dialog = NULL;
}

