#include "frame-config.h"
#include "rows/card-row.h"
#include "widgets/containerwidget.h"

#include "i18n.h"

FrameConfig::FrameConfig(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x) :
Gtk::Widget(cobject) {
    cardsBox = x->get_widget<Gtk::Box>("cardsBox");
    cardsContainer = ContainerWidget::create(_("Cards"), NULL, _("<i>No configurable hardware dectected.</i>"));
    cardsBox->append(*cardsContainer);
}

FrameConfig* FrameConfig::create() {
    FrameConfig* f;
    Glib::RefPtr<Gtk::Builder> x = Gtk::Builder::create_from_resource("/pulsecontrol/ui/frames/frame-config.ui");
    f = Gtk::Builder::get_widget_derived<FrameConfig>(x, "configframe");
    f->reference();
    return f;
}

void FrameConfig::updateCard(const pa_card_info &info) {
    if (cardWidgets.count(info.index))
        cardWidgets[info.index]->update(info);
    else {
        cardWidgets[info.index] = CardRow::create(cardsContainer, info);
    }
}

void FrameConfig::updateCardCodecs(const std::string& card_name, const std::unordered_map<std::string, std::string>& codecs) {
    for (auto c : cardWidgets) {
        if (c.second != NULL)
            c.second->updateCardCodecs(card_name, codecs);
    }
}

void FrameConfig::setActiveCodec(const std::string& card_name, const std::string& codec) {
    for (auto c : cardWidgets) {
        if (c.second != NULL)
            c.second->setActiveCodec(card_name, codec);
    }
}

void FrameConfig::setCardProfileIsSticky(const std::string& card_name, gboolean profile_is_sticky) {
    for (auto c : cardWidgets) {
        if (c.second != NULL)
            c.second->setCardProfileIsSticky(card_name, profile_is_sticky);
    }
}


void FrameConfig::removeCard(uint32_t index) {
    if (!cardWidgets.count(index))
        return;

    delete cardWidgets[index];
    cardWidgets.erase(index);
}

void FrameConfig::removeAllCards() {
    while (!cardWidgets.empty())
        removeCard(cardWidgets.begin()->first);
};


std::map<std::string,int64_t> FrameConfig::findPortLatencyOffsetForCard(uint32_t card_index) {
    std::map<uint32_t, CardRow*>::iterator cw = cardWidgets.find(card_index);
    if (cw != cardWidgets.end()){
        return cw->second->get_latency_offset_for_all_ports();
    }
    else {
        std::map<std::string,int64_t> result;
        result.clear();
        return result;
    }
}
