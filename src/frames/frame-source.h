#ifndef frame_source_h
#define frame_source_h

#include "pulsecontrol.h"

class SourceRow;
class ContainerWidget;

class FrameSource : public Gtk::Widget {
public:
    FrameSource(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x, gint showSourceType);
    static FrameSource* create(gint showSourceType);

    void updateSource(const pa_source_info &info);
    void updateServer(const pa_server_info &info);
    void updateVolumeMeter(uint32_t source_index, double v);
    void updatePortsLatencyOffsetForCard(uint32_t card_index, std::map<std::string,int64_t> port_latency_offsets);
    void removeSource(uint32_t index);
    void removeAllSources();

    void setCanRenameDevices(bool value);

    gint getSourceTypeShown();
    std::list<DeviceComboBoxItem> makeDeviceComboBox();
protected:
    Gtk::Box *sourcesBox;
    Gtk::Widget *noSourcesLabel;
    ContainerWidget *hardwareSourcesContainer, *virtualSourcesContainer, *monitorsContainer;
    Gtk::ToggleButton *showHardwareSourcesButton, *showVirtualSourcesButton, *showMonitorsButton;

    std::map<uint32_t, SourceRow*> sourceWidgets;
    std::map<uint32_t, std::map<std::string,int64_t>> card_port_latency_offsets;
    Glib::ustring defaultSourceName;
    bool canRenameDevices;

    ContainerWidget* findContainer(const pa_source_info &info);
    virtual void onVisibleSourceTypeChanged();
};

#endif
