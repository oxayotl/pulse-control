#ifndef frame_sink_h
#define frame_sink_h

#include "pulsecontrol.h"

class SinkRow;
class ContainerWidget;
class CreateVirtualSinkDialog;

class FrameSink : public Gtk::Widget {
public:
    FrameSink(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x, gint showSinkType);
    static FrameSink* create(gint showSinkType);

    bool updateSink(const pa_sink_info &info);
    void updateServer(const pa_server_info &info);
    void updateVolumeMeter(uint32_t source_index, double v);
#if HAVE_EXT_DEVICE_RESTORE_API
    void updateDeviceInfo(const pa_ext_device_restore_info &info);
#endif
    void updatePortsLatencyOffsetForCard(uint32_t card_index, std::map<std::string,int64_t> port_latency_offsets);

    void removeSink(uint32_t index);
    void removeAllSinks();

    void setCanRenameDevices(bool value);

    uint32_t getMonitorForSink(uint32_t sink_idx);
    gint getSinkTypeShown();
    std::list<DeviceComboBoxItem> makeDeviceComboBox();
protected:
    Gtk::Box *sinksBox;
    Gtk::Widget *noSinksLabel;
    ContainerWidget *hardwareSinksContainer, *virtualSinksContainer;
    Gtk::ToggleButton *showHardwareSinksButton, *showVirtualSinksButton;

    std::map<uint32_t, SinkRow*> sinkWidgets;
    std::map<uint32_t, std::map<std::string,int64_t>> card_port_latency_offsets;
    Glib::ustring defaultSinkName;
    bool canRenameDevices;

    ContainerWidget* findContainer(const pa_sink_info &info);
    virtual void onVisibleSinkTypeChanged();

    CreateVirtualSinkDialog *create_virtual_sink_dialog;
    void openCreateVirtualSinkDialog();
    void onCloseCreateVirtualSinkDialog();
};

#endif
