#include "frame-source.h"
#include "rows/source-row.h"
#include "widgets/containerwidget.h"

#include "i18n.h"

FrameSource::FrameSource(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x, gint showSourceType) :
Gtk::Widget(cobject),
canRenameDevices(false) {
    sourcesBox = x->get_widget<Gtk::Box>("sourcesBox");
    noSourcesLabel = x->get_widget<Gtk::Widget>("noSourcesLabel");

    hardwareSourcesContainer = ContainerWidget::create(_("Hardware Input Devices"), NULL,
    _("<i>No audio recording hardware dectected.</i>"), _("Unplugged devices"), "unplugged-symbolic");
    sourcesBox->append(*hardwareSourcesContainer);
    virtualSourcesContainer = ContainerWidget::create(_("Virtual Input Devices"), NULL,
    _("<i>No virtual sources.</i>"), _("Unplugged devices"), "unplugged-symbolic");
    sourcesBox->append(*virtualSourcesContainer);
    monitorsContainer = ContainerWidget::create(_("Monitors"), NULL, _("<i>No monitors.</i>"));
    sourcesBox->append(*monitorsContainer);


    showHardwareSourcesButton = x->get_widget<Gtk::ToggleButton>("showHardwareSourcesButton");
    showVirtualSourcesButton = x->get_widget<Gtk::ToggleButton>("showVirtualSourcesButton");
    showMonitorsButton = x->get_widget<Gtk::ToggleButton>("showMonitorsButton");

    if (showSourceType != -1) {
        showHardwareSourcesButton->set_active(showSourceType & SOURCE_HARDWARE);
        showVirtualSourcesButton->set_active(showSourceType & SOURCE_VIRTUAL);
        showMonitorsButton->set_active(showSourceType & SOURCE_MONITOR);
    }

    showHardwareSourcesButton->signal_toggled().connect(sigc::mem_fun(*this, &FrameSource::onVisibleSourceTypeChanged));
    showVirtualSourcesButton->signal_toggled().connect(sigc::mem_fun(*this, &FrameSource::onVisibleSourceTypeChanged));
    showMonitorsButton->signal_toggled().connect(sigc::mem_fun(*this, &FrameSource::onVisibleSourceTypeChanged));
    onVisibleSourceTypeChanged();
}

FrameSource* FrameSource::create(gint showSourceType) {
    FrameSource* f;
    Glib::RefPtr<Gtk::Builder> x = Gtk::Builder::create_from_resource("/pulsecontrol/ui/frames/frame-source.ui");
    f = Gtk::Builder::get_widget_derived<FrameSource>(x, "sourcesframe", showSourceType);
    f->reference();
    return f;
}

void FrameSource::updateSource(const pa_source_info &info) {
    SourceRow *w;

    if (sourceWidgets.count(info.index)) {
        w = sourceWidgets.at(info.index);
    }
    else {
        w = SourceRow::create(findContainer(info), canRenameDevices, info);
        sourceWidgets.emplace(info.index,w);
    }

    w->updateSource(info);
    if (info.active_port != NULL) {
        std::string active_port = info.active_port->name;
        if (card_port_latency_offsets[info.card].count(active_port)) {
            w->setLatencyOffset(card_port_latency_offsets[info.card][active_port]);
        }
    }
    w->setIsDefault(defaultSourceName);
}

void FrameSource::updateServer(const pa_server_info &info) {
    defaultSourceName = info.default_source_name ? info.default_source_name : "";

    for (std::map<uint32_t, SourceRow*>::iterator i = sourceWidgets.begin(); i != sourceWidgets.end(); ++i) {
        SourceRow *w = i->second;

        if (!w)
            continue;

        w->setIsDefault(defaultSourceName);
    }
}

void FrameSource::updateVolumeMeter(uint32_t source_index, double v) {
    for (std::map<uint32_t, SourceRow*>::iterator i = sourceWidgets.begin(); i != sourceWidgets.end(); ++i) {
        SourceRow* w = i->second;

        if (w->index == source_index)
            w->updatePeak(v);
    }
}

void FrameSource::updatePortsLatencyOffsetForCard(uint32_t card_index, std::map<std::string,int64_t> port_latency_offsets) {
    card_port_latency_offsets[card_index] = port_latency_offsets;
    for (auto it = sourceWidgets.begin() ; it != sourceWidgets.end(); it++) {
        SourceRow *sw = it->second;
        if (sw->card_index == card_index) {
            if (port_latency_offsets.count(sw->activePort)) {
                sw->setLatencyOffset(port_latency_offsets[sw->activePort]);
            }
        }
    }
}


void FrameSource::removeSource(uint32_t index) {
    if (!sourceWidgets.count(index))
        return;
    SourceRow* w = sourceWidgets.at(index);
    w->container->removeWidget(w);
    sourceWidgets.erase(index);
}

void FrameSource::removeAllSources() {
    while (!sourceWidgets.empty())
         removeSource(sourceWidgets.begin()->first);
}


void FrameSource::setCanRenameDevices(bool value) {
    canRenameDevices = value;
    for (std::map<uint32_t, SourceRow*>::iterator i = sourceWidgets.begin(); i != sourceWidgets.end(); ++i) {
        SourceRow *w = i->second;
        w->canRenameDevice(value);
    }
}


gint FrameSource::getSourceTypeShown() {
    int showSourceType = SOURCE_NOTHING;
    if (showHardwareSourcesButton->get_active()) {
        showSourceType |= SOURCE_HARDWARE;
    }
    if (showVirtualSourcesButton->get_active()) {
        showSourceType |= SOURCE_VIRTUAL;
    }
    if (showMonitorsButton->get_active()) {
        showSourceType |= SOURCE_MONITOR;
    }
    return showSourceType;
}

std::list<DeviceComboBoxItem> FrameSource::makeDeviceComboBox() {
    std::list<DeviceComboBoxItem> result;
    for (auto i = sourceWidgets.begin(); i != sourceWidgets.end(); i++) {
        SourceRow *source = i->second;
        if (source != NULL)
            result.push_front(*source->makeDeviceComboBoxItem());
    }
    return result;
}


ContainerWidget* FrameSource::findContainer(const pa_source_info &info) {
    return info.monitor_of_sink != PA_INVALID_INDEX
        ? monitorsContainer
        : (info.flags & PA_SOURCE_HARDWARE ? hardwareSourcesContainer : virtualSourcesContainer);
}

void FrameSource::onVisibleSourceTypeChanged() {
    hardwareSourcesContainer->set_visible(showHardwareSourcesButton->get_active());
    virtualSourcesContainer->set_visible(showVirtualSourcesButton->get_active());
    monitorsContainer->set_visible(showMonitorsButton->get_active());
    noSourcesLabel->set_visible(!showHardwareSourcesButton->get_active() && !showVirtualSourcesButton->get_active() && !showMonitorsButton->get_active());
}


