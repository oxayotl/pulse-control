#ifndef create_virtual_sink_dialog_h
#define create_virtual_sink_dialog_h

#include "pulsecontrol.h"

class PulseControlDropDown;

class CreateVirtualSinkDialog : public Gtk::Window {
public:
    CreateVirtualSinkDialog(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x,std::list<DeviceComboBoxItem> sinks);
    static CreateVirtualSinkDialog* create(std::list<DeviceComboBoxItem> sinks);
    void addSink(DeviceComboBoxItem *sink);
    void removeSink(Glib::ustring sink_name);

protected:
    Gtk::Window *dialog;
    Gtk::Box *box;
    PulseControlDropDown *firstDropDown;
    PulseControlDropDown *secondDropDown;
    Gtk::Widget *goToCombinedSinkButton;
    GtkWidget *navigationView;

    void createNullVirtualSink();
    void createCombinedSink();

    bool on_key_press_event(guint keyval, guint keycode, Gdk::ModifierType state);
    void onCombinedDropdownChange();

};

#endif
