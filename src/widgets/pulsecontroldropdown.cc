#include "pulsecontroldropdown.h"

#include "i18n.h"

PulseControlDropDown::PulseControlDropDown(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x) :
Gtk::DropDown(cobject),
disabled_item_index(-1)
{
    auto factory = Gtk::SignalListItemFactory::create();
    factory->signal_setup().connect(sigc::bind(sigc::mem_fun(*this, &PulseControlDropDown::model_on_setup)));
    factory->signal_bind().connect(sigc::mem_fun(*this, &PulseControlDropDown::model_on_bind));
    factory->signal_unbind().connect(sigc::mem_fun(*this, &PulseControlDropDown::on_unbind_list_item));
    set_list_factory(factory);
    factory = Gtk::SignalListItemFactory::create();
    factory->signal_setup().connect(sigc::bind(sigc::mem_fun(*this, &PulseControlDropDown::model_on_setup_selected_item)));
    factory->signal_bind().connect(sigc::mem_fun(*this, &PulseControlDropDown::model_on_bind_selected_item));
    set_factory(factory);
    auto model = Gio::ListStore<PulseControlDropDownItem>::create();
    set_model(model);
}

PulseControlDropDown* PulseControlDropDown::create() {
    PulseControlDropDown* d;
    Glib::RefPtr<Gtk::Builder> x = Gtk::Builder::create_from_resource("/pulsecontrol/ui/widgets/pulsecontroldropdown.ui");

    d = Gtk::Builder::get_widget_derived<PulseControlDropDown>(x, "sinkDropdown");
    d->reference();

    return d;
}

Glib::RefPtr<Gio::ListStore<PulseControlDropDownItem>> PulseControlDropDown::get_list_store_model() {
    return std::dynamic_pointer_cast<Gio::ListStore<PulseControlDropDownItem>>(get_model());
}

void PulseControlDropDown::append(Glib::ustring id, Glib::ustring text, guint priority, gboolean availability){
    get_list_store_model()->insert_sorted(PulseControlDropDownItem::create(id, text, priority, availability), sigc::mem_fun(*this, &PulseControlDropDown::sort_function));
}

void PulseControlDropDown::set_active_id(Glib::ustring id){
    auto model = get_list_store_model();
    for (uint32_t i = 0; i < model->get_n_items(); i++) {
        if (model->get_item(i) != NULL && id == model->get_item(i)->m_id) {
            set_selected(i);
            return;
        }
    }
    g_debug(_("Could not set active id %s in pulsecontroldrown, id was not present in model"), id.c_str());
}

guint PulseControlDropDown::get_item_number() {
    auto model = get_list_store_model();
    return model->get_n_items();
}

void PulseControlDropDown::remove_id(Glib::ustring id){
    auto model = get_list_store_model();
    for (uint32_t i = 0; i < model->get_n_items(); i++) {
        if (model->get_item(i) != NULL && id == model->get_item(i)->m_id) {
            model->remove(i);
            return;
        }
    }
    g_debug(_("Could not remove id %s in pulsecontroldrown, id was not present in model"), id.c_str());
}

void PulseControlDropDown::remove_all(){
    get_list_store_model()->remove_all();
}

void PulseControlDropDown::set_disabled_item(guint index){
    disabled_item_index = index;
    signal_disabled_item_changed.emit();
}

Glib::ustring PulseControlDropDown::get_active_id(){
    Glib::RefPtr<PulseControlDropDownItem> selected = std::dynamic_pointer_cast<PulseControlDropDownItem>(get_selected_item());
    if (selected == NULL) {
        return NULL;
    }
    return selected->m_id;
}

void PulseControlDropDown::model_on_setup_selected_item(const Glib::RefPtr<Gtk::ListItem>& list_item)
{
  list_item->set_child(*Gtk::make_managed<Gtk::Label>());
}


void PulseControlDropDown::model_on_bind_selected_item(const Glib::RefPtr<Gtk::ListItem>& list_item)
{
    auto col = std::dynamic_pointer_cast<PulseControlDropDownItem>(list_item->get_item());
    auto label = dynamic_cast<Gtk::Label*>(list_item->get_child());
    if (!label)
        return;
    label->set_text(col->m_text);
}


void PulseControlDropDown::model_on_setup(const Glib::RefPtr<Gtk::ListItem>& list_item)
{
    auto hbox = Gtk::make_managed<Gtk::Box>(Gtk::Orientation::HORIZONTAL, 10);
    auto label = Gtk::make_managed<Gtk::Label>("");
    label->set_halign(Gtk::Align::END);
    hbox->append(*label);

    auto checkmark = Gtk::make_managed<Gtk::Image>();
    checkmark->set_from_icon_name("object-select-symbolic");
    checkmark->set_visible(false);
    hbox->append(*checkmark);

    list_item->set_child(*hbox);
}

void PulseControlDropDown::model_on_bind(const Glib::RefPtr<Gtk::ListItem>& list_item)
{
    auto col = std::dynamic_pointer_cast<PulseControlDropDownItem>(list_item->get_item());
    if (!col)
        return;
    auto hbox = dynamic_cast<Gtk::Box*>(list_item->get_child());

    auto label = dynamic_cast<Gtk::Label*>(hbox->get_first_child());
    if (!label)
        return;

    if (col->m_availability)
        label->set_text(Glib::ustring::format(col->m_text));
    else
        label->set_markup("<i>" + Glib::ustring::format(col->m_text) + "</i>");

    auto checkmark = dynamic_cast<Gtk::Image*>(hbox->get_last_child());
    if (!checkmark)
        return;


    auto connection = property_selected_item().signal_changed().connect(sigc::bind(sigc::mem_fun(*this, &PulseControlDropDown::on_selected_item_changed), list_item));
    list_item->set_data("connection", new sigc::connection(connection), Glib::destroy_notify_delete<sigc::connection>);
    on_selected_item_changed(list_item);

    auto connection2 = signal_disabled_item_changed.connect(sigc::bind(sigc::mem_fun(*this, &PulseControlDropDown::on_disabled_item_changed), list_item));
    list_item->set_data("connection2", new sigc::connection(connection2), Glib::destroy_notify_delete<sigc::connection>);
    on_disabled_item_changed(list_item);
}

void PulseControlDropDown::on_unbind_list_item(const Glib::RefPtr<Gtk::ListItem>& list_item)
{
    if (auto connection = static_cast<sigc::connection*>(list_item->get_data("connection")))
    {
        connection->disconnect();
        list_item->set_data("connection", nullptr);
    }
    if (auto connection2 = static_cast<sigc::connection*>(list_item->get_data("connection2")))
    {
        connection2->disconnect();
        list_item->set_data("connection2", nullptr);
    }
}

void PulseControlDropDown::on_selected_item_changed(const Glib::RefPtr<Gtk::ListItem>& list_item)
{
    auto hbox = dynamic_cast<Gtk::Box*>(list_item->get_child());
    if (!hbox)
        return;
    auto checkmark = dynamic_cast<Gtk::Image*>(hbox->get_last_child());
    if (!checkmark)
        return;
    checkmark->set_visible(get_selected_item() == list_item->get_item());
}

gint PulseControlDropDown::sort_function (const Glib::RefPtr<const PulseControlDropDownItem>& item1, const Glib::RefPtr<const PulseControlDropDownItem>& item2) {
    gint result = 0;

    if (item1->m_availability != item2->m_availability) {
        result = item1->m_availability ? -1 : 1;
    }

    if (result == 0) {
        result = item2->m_priority - item1->m_priority;
    }

    if (result == 0) {
        result = strcmp(item1->m_text.c_str(), item2->m_text.c_str());
    }

    return result;
}

void PulseControlDropDown::on_disabled_item_changed(const Glib::RefPtr<Gtk::ListItem>& list_item)
{
    list_item->set_selectable(list_item->get_position() != disabled_item_index);
    list_item->get_child()->set_sensitive(list_item->get_position() != disabled_item_index);
}
