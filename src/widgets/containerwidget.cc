#include "containerwidget.h"
#include "i18n.h"

/*** ContainerWidget ***/
ContainerWidget::ContainerWidget(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x) :
    Gtk::Widget(cobject) {
    listBox = x->get_widget<Gtk::ListBox>("listBox");
    placeholderLabel = x->get_widget<Gtk::Label>("placeholderLabel");
    preferenceGroup = ADW_PREFERENCES_GROUP(gtk_builder_get_object (x->gobj(), "preferenceGroup"));

    expanderRow = adw_expander_row_new();
    gtk_list_box_row_set_selectable(GTK_LIST_BOX_ROW(expanderRow), false);
    gtk_widget_set_visible(GTK_WIDGET(expanderRow), false);

    gtk_list_box_append(GTK_LIST_BOX(listBox->gobj()), expanderRow);
}

ContainerWidget* ContainerWidget::create(const char* title, const char* description, const char* placeholder, const char* expanderRowLabel, const char* expanderRowIcon) {
    ContainerWidget* w;
    Glib::RefPtr<Gtk::Builder> x = Gtk::Builder::create_from_resource("/pulsecontrol/ui/widgets/containerwidget.ui");
    w = Gtk::Builder::get_widget_derived<ContainerWidget>(x, "containerWidget");

    if (title)
        adw_preferences_group_set_title (ADW_PREFERENCES_GROUP(w->preferenceGroup), title);
    if (description)
        adw_preferences_group_set_description(ADW_PREFERENCES_GROUP(w->preferenceGroup), description);
    if (placeholder)
        w->placeholderLabel->set_label(placeholder);
    w->reference();
    if (expanderRowLabel)
        adw_preferences_row_set_title(ADW_PREFERENCES_ROW(w->expanderRow), expanderRowLabel);
    if (expanderRowIcon) {
        GtkWidget* icon = gtk_image_new_from_icon_name (expanderRowIcon);
        adw_expander_row_add_prefix(ADW_EXPANDER_ROW(w->expanderRow), icon);
    }

    return w;
}


ContainerWidget::~ContainerWidget() {
}

void ContainerWidget::addSuffix(Gtk::Widget *widget){
    adw_preferences_group_set_header_suffix (ADW_PREFERENCES_GROUP(preferenceGroup), widget->gobj());
};

void ContainerWidget::addWidget(Gtk::Widget *widget){
    visibleWidgets.insert(widget);
    listBox->prepend(*widget);
};

void ContainerWidget::addHiddenWidget(Gtk::Widget *widget){
    hiddenWidgets.insert(widget);
    adw_expander_row_add_row (ADW_EXPANDER_ROW(expanderRow), GTK_WIDGET(widget->gobj()));
    gtk_widget_set_visible(GTK_WIDGET(expanderRow), true);
};

void ContainerWidget::updateHiddenStatus(Gtk::Widget *widget, bool hidden){
    std::set<Gtk::Widget*> originSet = hidden ? visibleWidgets : hiddenWidgets;
    std::set<Gtk::Widget*> destinationSet = !hidden ? visibleWidgets : hiddenWidgets;
    if (!originSet.count(widget)) {
        if (!destinationSet.count(widget))
            g_debug(_("Tried to update hidden status of a widget not present in the containerwidget"));
    }
    else {
        widget->reference();
        removeWidget(widget);
        if (hidden) {
            addHiddenWidget(widget);
        }
        else {
            addWidget(widget);
        }
        widget->unreference();
    }
};


void ContainerWidget::removeWidget(Gtk::Widget *widget){
    bool removed = false;
    if (visibleWidgets.count(widget)) {
        visibleWidgets.erase(widget);
        listBox->remove(*widget);
        removed = true;
    }
    if (hiddenWidgets.count(widget)) {
        hiddenWidgets.erase(widget);
        adw_expander_row_remove (ADW_EXPANDER_ROW(expanderRow),GTK_WIDGET(widget->gobj()));
        gtk_widget_set_visible(GTK_WIDGET(expanderRow), !hiddenWidgets.empty());
        removed = true;
    }
    if (!removed)
        g_debug(_("Tried to remove a widget from a containerwidget that didn't contain it"));
};

