#ifndef port_drop_down_h
#define port_drop_down_h

#include "pulsecontrol.h"

class PulseControlDropDownItem : public Glib::Object
{
    public:
    Glib::ustring m_id;
    Glib::ustring m_text;
    guint m_priority;
    gboolean m_availability;

    static Glib::RefPtr<PulseControlDropDownItem> create(const Glib::ustring& id, const Glib::ustring& text, const guint priority, const gboolean availability)
    {
        return Glib::make_refptr_for_instance<PulseControlDropDownItem>(new PulseControlDropDownItem(id, text, priority, availability));
    }

    protected:
    PulseControlDropDownItem(const Glib::ustring& id, const Glib::ustring& text, const guint priority, const gboolean availability)
    : m_id(id), m_text(text), m_priority(priority), m_availability(availability)
    {}
};

class PulseControlDropDown : public Gtk::DropDown {
public:
    PulseControlDropDown(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x);
    static PulseControlDropDown* create();

    void append(Glib::ustring id, Glib::ustring text, guint priority, gboolean availability);
    void set_active_id(Glib::ustring id);
    guint get_item_number();
    void remove_id(Glib::ustring id);
    void remove_all();
    void set_disabled_item(guint);
    Glib::ustring get_active_id();

 protected:
    Glib::RefPtr<Gio::ListStore<PulseControlDropDownItem>> get_list_store_model();
    void model_on_setup(const Glib::RefPtr<Gtk::ListItem>& list_item);
    void model_on_bind(const Glib::RefPtr<Gtk::ListItem>& list_item);
    void on_unbind_list_item(const Glib::RefPtr<Gtk::ListItem>& list_item);
    void on_selected_item_changed(const Glib::RefPtr<Gtk::ListItem>& list_item);
    void model_on_setup_selected_item(const Glib::RefPtr<Gtk::ListItem>& list_item);
    void model_on_bind_selected_item(const Glib::RefPtr<Gtk::ListItem>& list_item);
    gint sort_function (const Glib::RefPtr<const PulseControlDropDownItem>& item1, const Glib::RefPtr<const PulseControlDropDownItem>& item2);

    sigc::signal<void()> signal_disabled_item_changed;
    void on_disabled_item_changed(const Glib::RefPtr<Gtk::ListItem>& list_item);
    guint disabled_item_index;
};

#endif

