#ifndef containerwidget_h
#define containerwidget_h

#include "pulsecontrol.h"

// class ContainerWidget : public Gtk::Frame {
class ContainerWidget : public Gtk::Widget {
public:
    ContainerWidget(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x);
    virtual ~ContainerWidget();

    static ContainerWidget* create(const char* title, const char* description, const char* placeholder,
                                   const char* expanderRowLabel = NULL, const char* expanderRowIcon = NULL);

    void addSuffix(Gtk::Widget *widget);
    void addWidget(Gtk::Widget *widget);
    void addHiddenWidget(Gtk::Widget *widget);
    void updateHiddenStatus(Gtk::Widget *widget, bool hidden);
    void removeWidget(Gtk::Widget *widget);

protected:
    Gtk::Label *title, *description;
    Gtk::ListBox *listBox;
    Gtk::Label *placeholderLabel;
    GtkWidget *expanderRow;
    AdwPreferencesGroup *preferenceGroup;

    std::set<Gtk::Widget*> visibleWidgets;
    std::set<Gtk::Widget*> hiddenWidgets;
};

#endif
