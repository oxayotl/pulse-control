#include "create-virtual-sink-dialog.h"
#include "containerwidget.h"
#include "pulsecontroldropdown.h"

#include "i18n.h"


CreateVirtualSinkDialog::CreateVirtualSinkDialog(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x, std::list<DeviceComboBoxItem> sinks) :
Gtk::Window(cobject) {
    firstDropDown = Gtk::Builder::get_widget_derived<PulseControlDropDown>(x, "firstDropDown");
    secondDropDown = Gtk::Builder::get_widget_derived<PulseControlDropDown>(x, "secondDropDown");

    goToCombinedSinkButton = x->get_widget<Gtk::Widget>("go-to-combined-sink-button");

    for (DeviceComboBoxItem sink : sinks) {
        firstDropDown->append(sink.name, sink.description, sink.priority, sink.availability);
        secondDropDown->append(sink.name, sink.description, sink.priority, sink.availability);
    }
    if (sinks.size() < 2) {
        goToCombinedSinkButton->set_sensitive(false);
    }
    else {
        firstDropDown->set_selected(0);
        secondDropDown->set_selected(1);
    }
    firstDropDown->connect_property_changed("selected", sigc::mem_fun(*this, &CreateVirtualSinkDialog::onCombinedDropdownChange));
    secondDropDown->connect_property_changed("selected", sigc::mem_fun(*this, &CreateVirtualSinkDialog::onCombinedDropdownChange));
    onCombinedDropdownChange();


    Gtk::Button *nullSinkButton = x->get_widget<Gtk::Button>("null-sink-button");
    nullSinkButton->signal_clicked().connect(sigc::mem_fun(*this, &CreateVirtualSinkDialog::createNullVirtualSink));
    Gtk::Button *combineSinkButton = x->get_widget<Gtk::Button>("combined-sink-button");
    combineSinkButton->signal_clicked().connect(sigc::mem_fun(*this, &CreateVirtualSinkDialog::createCombinedSink));

    navigationView = x->get_widget<Gtk::Widget>("navigationView")->gobj();

    auto event_controller_key = Gtk::EventControllerKey::create();
    event_controller_key->signal_key_pressed().connect(sigc::mem_fun(*this, &CreateVirtualSinkDialog::on_key_press_event), false);
    this->add_controller(event_controller_key);

}

CreateVirtualSinkDialog* CreateVirtualSinkDialog::create(std::list<DeviceComboBoxItem> sinks) {
    CreateVirtualSinkDialog* d;
    Glib::RefPtr<Gtk::Builder> x = Gtk::Builder::create_from_resource("/pulsecontrol/ui/widgets/create-virtual-sink-dialog.ui");

    d = Gtk::Builder::get_widget_derived<CreateVirtualSinkDialog>(x, "dialog", sinks);

    // d->reference();
    return d;
}

void CreateVirtualSinkDialog::addSink(DeviceComboBoxItem *sink) {
    firstDropDown->append(sink->name, sink->description, sink->priority, sink->availability);
    secondDropDown->append(sink->name, sink->description, sink->priority, sink->availability);
    if (firstDropDown->get_active_id() == secondDropDown->get_active_id()) {
        secondDropDown->set_active_id(sink->name);
    }
    goToCombinedSinkButton->set_sensitive(firstDropDown->get_item_number() >= 2);
}

void CreateVirtualSinkDialog::removeSink(Glib::ustring sink_name) {
    firstDropDown->remove_id(sink_name);
    secondDropDown->remove_id(sink_name);
    if (firstDropDown->get_item_number() < 2) {
        goToCombinedSinkButton->set_sensitive();
        AdwNavigationPage* visiblePage = adw_navigation_view_get_visible_page (ADW_NAVIGATION_VIEW(navigationView));
        const char* tag = adw_navigation_page_get_tag(visiblePage);
        if (tag) {
            std::cout << "Tag : " << tag << std::endl;
            if (strcmp(tag, "combine-sink") == 0) {
                adw_navigation_view_pop(ADW_NAVIGATION_VIEW(navigationView));
            }
        }
    }
}


void CreateVirtualSinkDialog::createNullVirtualSink() {
// TODO Allow naming
    pa_operation *o = pa_context_load_module(get_context(), "module-null-sink", NULL, NULL, NULL);
    if (o)
        pa_operation_unref(o);
    close();
}


void CreateVirtualSinkDialog::createCombinedSink() {
    Glib::ustring firstSink = firstDropDown->get_active_id();
    Glib::ustring secondSink = secondDropDown->get_active_id();
    pa_operation *o = pa_context_load_module(get_context(), "module-combine-sink", ("slaves=" + firstSink + "," + secondSink).c_str(), NULL, NULL);
    if (o)
        pa_operation_unref(o);
    close();
}

bool CreateVirtualSinkDialog::on_key_press_event(guint keyval, guint keycode, Gdk::ModifierType state) {
    if (keyval == GDK_KEY_Escape) {
        close();
    }
    return false;
}

void CreateVirtualSinkDialog::onCombinedDropdownChange() {
    firstDropDown->set_disabled_item(secondDropDown->get_selected());
    secondDropDown->set_disabled_item(firstDropDown->get_selected());
}
