/***
  This file is part of pulsecontrol.

  Copyright 2006-2008 Lennart Poettering
  Copyright 2009 Colin Guthrie

  pulsecontrol is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  pulsecontrol is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with pulsecontrol. If not, see <http://www.gnu.org/licenses/>.
***/

#ifndef mainwindow_h
#define mainwindow_h

class MainWindow;

#include "pulsecontrol.h"
#include <pulse/ext-stream-restore.h>
#if HAVE_EXT_DEVICE_RESTORE_API
#  include <pulse/ext-device-restore.h>
#endif

#include <canberra.h>
#include <adwaita.h>

#include <unordered_map>

class FrameSinkInput;
class FrameSourceOutput;
class FrameSink;
class FrameSource;
class FrameConfig;

class ContainerWidget;

class MainWindow : public Gtk::Window {
public:
    MainWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& x);
    static MainWindow* create(bool maximize);
    virtual ~MainWindow();

    void updateCard(const pa_card_info &info);
    bool updateSink(const pa_sink_info &info);
    void updateSource(const pa_source_info &info);
    void updateSinkInput(const pa_sink_input_info &info);
    void updateSourceOutput(const pa_source_output_info &info);
    void updateClient(const pa_client_info &info);
    void updateServer(const pa_server_info &info);
    void updateVolumeMeter(uint32_t source_index, uint32_t sink_input_index, double v);
    void updateRole(const pa_ext_stream_restore_info &info);
#if HAVE_EXT_DEVICE_RESTORE_API
    void updateDeviceInfo(const pa_ext_device_restore_info &info);
#endif
    void updateCardCodecs(const std::string& card_name, const std::unordered_map<std::string, std::string>& codecs);
    void setActiveCodec(const std::string& card_name, const std::string& codec);

    void setCardProfileIsSticky(const std::string& card_name, gboolean profile_is_sticky);

    void removeCard(uint32_t index);
    void removeSink(uint32_t index);
    void removeSource(uint32_t index);
    void removeSinkInput(uint32_t index);
    void removeSourceOutput(uint32_t index);
    void removeClient(uint32_t index);

    void selectTab(int tab_number);

    void removeAllWidgets();

    void setConnectingMessage(const char *string = NULL);

    Gtk::Widget *viewStack;
    Gtk::Box *streamsBox, *recsBox, *sinksBox, *sourcesBox, *configBox;
    Gtk::Label *connectingLabel;
    FrameSinkInput *sinkInputFrame;
    FrameSourceOutput *sourceOutputFrame;
    FrameSink *sinkFrame;
    FrameSource *sourceFrame;
    FrameConfig *configFrame;


    std::map<uint32_t, pa_stream*> sourcePeaks;
    std::map<uint32_t, std::pair<uint32_t, pa_stream*>> sinkInputPeaks;
    std::map<uint32_t, std::vector<uint32_t>> sink_inputs_without_monitor_by_sink;

    std::map<uint32_t, char*> clientNames;

    void setConnectionState(gboolean connected);
    pa_stream* createMonitorStream(uint32_t source_idx, uint32_t stream_idx, bool suspend);
    void createMonitorStreamForSource(uint32_t source_idx, bool suspend);
    void createMonitorStreamForSinkInput(uint32_t sink_input_idx, uint32_t sink_idx);

    void deleteEventRoleRow();

    uint32_t moduleDeviceManagerIndex;
    void removedModule(uint32_t index);
    void setModuleDeviceManagerIndex(uint32_t index);

protected:
    virtual void on_realize();
    virtual bool on_key_press_event(guint keyval, guint keycode, Gdk::ModifierType state);

private:
    gboolean m_connected;
    gchar* m_config_filename;
};


#endif

